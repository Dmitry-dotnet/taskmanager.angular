﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Threading.Tasks;
using LinqBinary.BLL.Services.SpecialQuery.Interfaces;
using LinqBinary.Common.DTO;
using LinqBinary.Common.DTO.DataModels;
using Microsoft.AspNetCore.Mvc;

namespace LinqBinary.API.Controllers
{
    [Route("api/special")]
    [ApiController]
    public class SpecialQueriesController : Controller
    {
        ISpecialQueryService _service;

        public SpecialQueriesController(ISpecialQueryService service)
        {
            _service = service;
        }

        [HttpGet("dictionary/{id}")]
        public async Task<ActionResult<Dictionary<ProjectDTO, int>>> GetDictionaryOfUserProjects(int id)
        {
            return Ok(await _service.GetDictionaryOfUserProjects(id));
        }

        [HttpGet("userTasks/{id}")]
        public async Task<ActionResult<IEnumerable<ProjectTaskDTO>>> GetUsersTasks(int id)
        {
            return Ok(await _service.GetUsersTasks(id));
        }

        [HttpGet("finishedTasks/{id}")]
        public async Task<ActionResult<IEnumerable<FinishedTaskModel>>> GetUserFinishedTasks(int id)
        {
            return Ok(await _service.GetUserFinishedTasks(id));
        }
        
        [HttpGet("oldUsersTeam")]
        public async Task<ActionResult<IEnumerable<TeamWithUserModel>>> GetTeamsWithOldUsers()
        {
            return Ok(await _service.GetTeamsWithOldUsers());
        }
        [HttpGet("usersWithTasks")]
        public async Task<ActionResult<IEnumerable<UserTasksModel>>> GetUsersWithTasks()
        {
            return Ok(await _service.GetUsersWithTasks());
        }

        [HttpGet("userInfo/{id}")]
        public async Task<ActionResult<UserInfoModel>> GetUserInfoModel(int id)
        {
            try
            {
                return Ok(await _service.GetUserInfoModel(id));
            }
            catch (Exception)
            {
                return Ok("This user dont have any project");
                throw;
            }
        }

        [HttpGet("projectsInfo")]
        public async Task<ActionResult<IEnumerable<ProjectInfoModel>>> GetProjectsInfo()
        {
            return Ok(await _service.GetProjectsInfo());
        }

        [HttpGet("unfinishedTasks/{id}")]
        public async Task<ActionResult> GetUnFinishedTask(int id)
        {
            try
            {
                return Ok(await _service.UserUnfinishedTasks(id));
            }
            catch (NullReferenceException)
            {
                return NotFound();
                throw;
            }
        }
    }
}
