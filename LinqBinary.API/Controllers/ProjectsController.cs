﻿using System.Collections.Generic;
using System.Linq;
using LinqBinary.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using LinqBinary.Common.DTO;
using System;
using System.Threading.Tasks;

namespace LinqBinary.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IEntityService<ProjectDTO> _service;
        public ProjectsController(IEntityService<ProjectDTO> service)
        {
            _service = service;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> Get()
        {
            var res = await _service.GetAll();
            return Ok(res);
        }

        [HttpGet("{id}")]
        public async Task< ActionResult<ProjectDTO>> Get(int id)
        {
            try
            {
                var res = await _service.Get(id);
                return res;
            }
            catch (NullReferenceException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> Post([FromBody] ProjectDTO value)
        {
            return Created("", await _service.Create(value));
        }

        [HttpPut]
        public async Task<ActionResult<ProjectDTO>> Put([FromBody] ProjectDTO value)
        {
            try
            {
                await _service.Update(value);
                return value;
            }
            catch (NullReferenceException)
            {
                return NotFound();

            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return Ok();
        }
    }
}
