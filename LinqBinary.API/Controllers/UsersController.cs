﻿using System.Collections.Generic;
using System.Linq;
using LinqBinary.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using LinqBinary.Common.DTO;
using System;
using System.Threading.Tasks;

namespace LinqBinary.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IEntityService<UserDTO> _service;
        public UsersController(IEntityService<UserDTO> service)
        {
            _service = service;
        }
        [HttpGet]
        public async Task<ActionResult<List<UserDTO>>> Get()
        {
            var res = await _service.GetAll();
            return Ok(res);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> Get(int id)
        {
            try
            {
                var res = await _service.Get(id);
                return res;
            }
            catch (NullReferenceException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> Post([FromBody] UserDTO value)
        {
            return Ok(await _service.Create(value));
        }

        [HttpPut]
        public async Task<ActionResult<UserDTO>> Put([FromBody] UserDTO value)
        {
            try
            {
                await _service.Update(value);
                return value;
            }
            catch (NullReferenceException)
            {
                return NotFound();

            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {

            try
            {
                await _service.Delete(id);
                return Ok();
            }
            catch (NullReferenceException)
            {
                return NotFound();
            }
        }
    }
}
