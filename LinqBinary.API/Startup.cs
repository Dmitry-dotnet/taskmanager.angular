using AutoMapper;
using LinqBinary.BLL.MappingProfiles;
using LinqBinary.BLL.Services;
using LinqBinary.BLL.Services.Interfaces;
using LinqBinary.BLL.Services.SpecialQuery;
using LinqBinary.BLL.Services.SpecialQuery.Interfaces;
using LinqBinary.Common.DTO;
using LinqBinary.DAL.Context;
using LinqBinary.DAL.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace LinqBinary.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddCors();

            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<ProjectTaskProfile>();
            });
            services.AddMvc().AddNewtonsoftJson();
            services.AddDbContext<LinqBinaryDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("BinaryLinqDB")));

            services.AddTransient<IEntityService<ProjectDTO>, ProjectService>();
            services.AddTransient<IEntityService<ProjectTaskDTO>, ProjectTaskService>();
            services.AddTransient<IEntityService<UserDTO>, UserService>();
            services.AddTransient<IEntityService<TeamDTO>, TeamService>();

            services.AddTransient<ISpecialQueryService, SpecialQueryService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            app.UseRouting();
            app.UseCors(
                oprions => 
                oprions.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
            app.UseAuthorization();
            app.UseHttpsRedirection();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
