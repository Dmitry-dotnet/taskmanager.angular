﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinqBinary.Common.DTO.Enums
{
    public enum TaskStateDTO
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
