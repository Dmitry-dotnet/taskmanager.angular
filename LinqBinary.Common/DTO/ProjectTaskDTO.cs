﻿using LinqBinary.Common.DTO.Enums;
using System;

namespace LinqBinary.Common.DTO
{
    public class ProjectTaskDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskStateDTO State { get; set; }
        public int? ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}
