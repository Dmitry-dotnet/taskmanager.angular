﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinqBinary.Common.DTO.DataModels
{
    public class TeamWithUserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserDTO> Users { get; set; }
    }
}
