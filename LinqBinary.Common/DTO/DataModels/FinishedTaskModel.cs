﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinqBinary.Common.DTO.DataModels
{
    public class FinishedTaskModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
