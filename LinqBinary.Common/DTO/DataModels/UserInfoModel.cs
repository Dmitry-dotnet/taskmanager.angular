﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinqBinary.Common.DTO.DataModels
{
    public class UserInfoModel
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int ProjectTasks { get; set; }
        public int CanceledOrUnFinishedTasks { get; set; }
        public ProjectTaskDTO LongestTask { get; set; }
    }
}
