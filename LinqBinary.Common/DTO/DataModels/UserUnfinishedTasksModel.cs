﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinqBinary.Common.DTO.DataModels
{
    public class UserUnfinishedTasksModel
    {
        public UserDTO User { get; set; }
        public List<ProjectTaskDTO>  Tasks { get; set; }
    }
}
