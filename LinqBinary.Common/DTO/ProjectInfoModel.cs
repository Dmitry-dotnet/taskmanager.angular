﻿namespace LinqBinary.Common.DTO
{
    public class ProjectInfoModel
    {
        public ProjectDTO Project { get; set; }
        public ProjectTaskDTO TaskWithLongestDescr { get; set; }
        public ProjectTaskDTO TaskWithShortestName { get; set; }

        public int? UsersInTeam { get; set; }
    }
}
