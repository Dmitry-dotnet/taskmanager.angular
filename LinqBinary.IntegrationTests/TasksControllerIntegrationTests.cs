﻿using LinqBinary.API;
using LinqBinary.Common.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LinqBinary.IntegrationTests
{   //TasksControllerIntegrationTests
    public class TasksControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private const string url = "http://localhost:64256/api/tasks/";
        public TasksControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }

        [Fact]
        public async Task DeleteTask()
        {
            var task = new ProjectTaskDTO()
            {
                Name = "test",
                PerformerId = 1
            };

            string jsonString = JsonConvert.SerializeObject(task);
            var postResponce = await _client.PostAsync(url
                , new StringContent(jsonString, Encoding.UTF8, "application/json"));
            var Createdteam = JsonConvert.DeserializeObject<ProjectTaskDTO>(await postResponce.Content.ReadAsStringAsync());

            var deleteResponce = await _client.DeleteAsync(url + $"{Createdteam.Id}");

            Assert.Equal(HttpStatusCode.OK, postResponce.StatusCode);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-10)]
        [InlineData(-100)]
        [InlineData(-1000)]
        public async Task DeleteTeam_WrongDeletingData(int id)
        {

            var response = await _client.DeleteAsync(url + $"{id}");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

    }
}
