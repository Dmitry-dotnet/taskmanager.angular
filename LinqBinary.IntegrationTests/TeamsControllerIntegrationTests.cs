﻿using LinqBinary.API;
using LinqBinary.Common.DTO;
using Microsoft.AspNetCore.Diagnostics;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LinqBinary.IntegrationTests
{
    public class TeamsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private const string url = "http://localhost:64256/api/teams/";
        public TeamsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }

        [Fact]
        public async Task AddingTeam()
        {
            TeamDTO team = new TeamDTO()
            {
                Name = "TestTeam"
            };
            string jsonString = JsonConvert.SerializeObject(team);
            var httpResponse = await _client.PostAsync(url
                , new StringContent(jsonString, Encoding.UTF8, "application/json"));
            //var Createdteam = JsonConvert.DeserializeObject<TeamDTO>(await httpResponse.Content.ReadAsStringAsync());

            //var deleteResponce = await _client.DeleteAsync($"http://localhost:64256/api/teams/{Createdteam.Id}");

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1000)]
        [InlineData(-10000)]
        public async Task DeleteTeam_WrongDeletingData(int id)
        {

            var response = await _client.DeleteAsync(url + $"{id}");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }



    }
}
