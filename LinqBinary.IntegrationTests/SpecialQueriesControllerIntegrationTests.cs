﻿using LinqBinary.API;
using LinqBinary.Common.DTO.DataModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LinqBinary.IntegrationTests
{
    public class SpecialQueriesControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private const string url = "http://localhost:64256/api/special/";
        public SpecialQueriesControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        [InlineData(10)]
        public async Task GetUserFinishedTasks_ReturnOk(int id)
        {
            var httpResponse = await _client.GetAsync(url + $"finishedTasks/{id}");
            //var res = JsonConvert.DeserializeObject<FinishedTaskModel>(await httpResponse.Content.ReadAsStringAsync());
           
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }


        [Theory]
        [InlineData(1)]
        public async Task SpecialTask_Success(int id)
        {
            var httpResponse = await _client.GetAsync(url + $"unfinishedTasks/{id}");
            var res = JsonConvert.DeserializeObject<UserUnfinishedTasksModel>(await httpResponse.Content.ReadAsStringAsync());

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(2, res.Tasks.Count);
        }
        [Theory]
        [InlineData(0)]
        public async Task SpecialTask_NotFound(int id)
        {
            var httpResponse = await _client.GetAsync(url + $"unfinishedTasks/{id}");

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);

        }
    }
}
