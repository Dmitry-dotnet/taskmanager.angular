﻿using LinqBinary.API;
using LinqBinary.Common.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LinqBinary.IntegrationTests
{
    public class ProjectsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private const string url = "http://localhost:64256/api/projects/";
        public ProjectsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }


        [Fact]
        public async Task CreateProject()
        {
            ProjectDTO project = new ProjectDTO()
            {
                CreationDate = DateTime.Now,
                Name = "Project",
                Description = "Description",
                AuthorId = 1,
                TeamId = 1
            };
            string jsonString = JsonConvert.SerializeObject(project);
            var httpResponse = await _client.PostAsync(url
                , new StringContent(jsonString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);

        }
    }
}
