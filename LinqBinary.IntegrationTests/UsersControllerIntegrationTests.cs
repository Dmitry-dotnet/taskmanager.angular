﻿using LinqBinary.API;
using LinqBinary.Common.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LinqBinary.IntegrationTests
{
    public class UsersControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {

        private readonly HttpClient _client;
        private const string url = "http://localhost:64256/api/users/";
        public UsersControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }

        [Fact]
        public async Task DeletingUser()
        {
            UserDTO team = new UserDTO()
            {
                FirstName = "TestUser",
                LastName = "Test",
                TeamId = 1
            };
            string jsonString = JsonConvert.SerializeObject(team);
            var httpResponse = await _client.PostAsync(url
                , new StringContent(jsonString, Encoding.UTF8, "application/json"));
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(await httpResponse.Content.ReadAsStringAsync());

            var deleteResponce = await _client.DeleteAsync(url + $"{createdUser.Id}");

            Assert.Equal(HttpStatusCode.OK, deleteResponce.StatusCode);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1000)]
        [InlineData(-10000)]
        public async Task DeleteUser_WrongDeletingData(int id)
        {

            var response = await _client.DeleteAsync(url + $"{id}");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

    }
}
