﻿using LinqBinary.Client.Logic;
using LinqBinary.Common.DTO;
using LinqBinary.Common.DTO.DataModels;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Timers;

namespace LinqBinary.Client
{
    public class Menu
    {
        private QueryService _queryService;
        private CrudService _crudService;
        private AsyncCompleter _asyncCompleter;
        private Timer _timer;
        bool _isTimerWorking = false;
        public Menu()
        {   
            _queryService = new QueryService();
            _crudService = new CrudService();
            _asyncCompleter = new AsyncCompleter();
            _timer = new Timer();
        }

        #region Menu Methods
        public async Task MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Welcome to Query Menu. Choose the action below:");
            Console.WriteLine("1 - CRUD-menu");
            Console.WriteLine("2 - Special Query Menu");
            Console.WriteLine("3 - Start async Task Completer");
            Console.WriteLine("4 - Exit");
            int value = CheckInput();
            switch (value)
            {
                case 1:
                    await CrudMenu();
                    break;
                case 2:
                    await QueryMenu();
                    break;
                case 3:
                    StartTimer();
                    await MainMenu();
                    break;
                case 4:
                    return;

            }
            
        }

        #region AsyncCompleter and timer

        private void StartTimer()
        {
            if (!_isTimerWorking)
            {
                _timer.Stop();
            }
            _timer.Elapsed += async (sender, e) => await _asyncCompleter.CompleteAsync();
            
            _timer.Interval = 5000;
            _isTimerWorking = true;
            _timer.Start();
        }


        #endregion

        #region Crud
        private async Task CrudMenu()
        {
            Console.Clear();
            Console.WriteLine("Welcome to Query Menu. Specify the object you want to work with:");
            Console.WriteLine("1 - Projects");
            Console.WriteLine("2 - Tasks");
            Console.WriteLine("3 - Users");
            Console.WriteLine("4 - Teamss");
            Console.WriteLine("5 - Go Back");
            int value = CheckInput();
            switch (value)
            {
                case 1:
                   await ProjectCrudMenu();
                    break;
                case 2:
                    await TaskCrudMenu();
                    break;
                case 3:
                    await UsersCrudMenu();
                    return;
                case 4:
                    await TeamsCrudMenu();
                    break;
                case 5:
                    await MainMenu();
                    break;
                default:
                    await CrudMenu();
                    break;
            }
        }

        private async Task ProjectCrudMenu()
        {
            Console.Clear();
            Console.WriteLine("Choose the action below for Project");
            Console.WriteLine("1 - Get all projects");
            Console.WriteLine("2 - Get project by id");
            Console.WriteLine("3 - add project");
            Console.WriteLine("4 - update project");
            Console.WriteLine("5 - delete project");
            Console.WriteLine("0 - Exit");
            int value = CheckInput();
            switch (value)
            {
                case 1:
                    await PrintAllProjects();
                    break;
                case 2:
                    await PrintProject();
                    break;
                case 3:
                    await CreateProject();
                    break;
                case 4:
                    await UpdateProject();
                    break;
                case 5:
                    await DeleteProject();
                    break;
                case 0:
                    await MainMenu();
                    return;
                default:
                    await ProjectCrudMenu();
                    break;

            }
        }

        private async Task PrintAllProjects()
        {
            var res =await _crudService.GetAllTasks();
            await PrintCollection(res);
        }

        private async Task PrintProject()
        {
            Console.WriteLine("Insert ID of project");
            int id = CheckInput();
            try
            {
                var res = await _crudService.GetProject(id);
                Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            }
            catch (Exception)
            {
                Error("Not Found");
                throw;
            }
            await BackToMenu();

        }

        private async Task CreateProject()
        {
            ProjectDTO project = new ProjectDTO();
            Console.WriteLine("Insert Autor Id");
            project.AuthorId = CheckInput();
            Console.WriteLine("Insert Team Id");
            project.TeamId = CheckInput();
            Console.WriteLine("Insert Name of Project");
            project.Name = Console.ReadLine();
            Console.WriteLine("Insert Description");
            project.Description = Console.ReadLine();
            project.CreationDate = DateTime.Now;
            var res = await _crudService.CreateProject(project);
            Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            await BackToMenu();
        }
        private async Task UpdateProject()
        {
            ProjectDTO project = new ProjectDTO();
            Console.WriteLine("Insert ID");
            project.Id = CheckInput();
            Console.WriteLine("Insert Autor Id");
            project.AuthorId = CheckInput();
            Console.WriteLine("Insert Team Id");
            project.TeamId = CheckInput();
            Console.WriteLine("Insert Name of Project");
            project.Name = Console.ReadLine();
            Console.WriteLine("Insert Description");
            project.Description = Console.ReadLine();
            project.CreationDate = DateTime.Now;
            var res = await _crudService.UpdateProject(project);
            Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            await BackToMenu();
        }

        private async Task DeleteProject()
        {
            Console.WriteLine("Insert ID of project");
            int id = CheckInput();
            try
            {
                var res = await _crudService.DeleteProject(id);
                Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            }
            catch (Exception)
            {
                Error("Not Found");
                throw;
            }
            await BackToMenu();

        }


        private async Task TaskCrudMenu()
        {
            Console.Clear();
            Console.WriteLine("Choose the action below for Task");
            Console.WriteLine("1 - Get all Tasks");
            Console.WriteLine("2 - Get Task by id");
            Console.WriteLine("3 - add Task");
            Console.WriteLine("4 - update Task");
            Console.WriteLine("5 - delete Task");
            Console.WriteLine("0 - Exit");
            int value = CheckInput();
            switch (value)
            {
                case 1:
                    await PrintAllTasks();
                    break;
                case 2:
                    await PrintTask();
                    break;
                case 3:
                    await CreateTask();
                    break;
                case 4:
                    await UpdateTask();
                    break;
                case 5:
                    await DeleteTask();
                    break;
                case 0:
                    await TaskCrudMenu();
                    break;
                default:
                    await MainMenu();
                    return;
                        
            }
        }

        private async Task PrintAllTasks()
        {
            var res = await _crudService.GetAllTasks();
            await PrintCollection(res);
        }

        private async Task PrintTask()
        {
            Console.WriteLine("Insert ID of Task");
            int id = CheckInput();
            try
            {
                var res = await _crudService.GetTask(id);
                Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            }
            catch (Exception)
            {
                Error("Not Found");
                throw;
            }
            await BackToMenu();
        }


        private async Task CreateTask()
        {
            ProjectTaskDTO task = new ProjectTaskDTO();
            Console.WriteLine("Insert Performer Id");
            task.PerformerId = CheckInput();
            Console.WriteLine("Insert Project Id");
            task.ProjectId = CheckInput();
            Console.WriteLine("Insert Name of Task");
            task.Name = Console.ReadLine();
            Console.WriteLine("Insert Description");
            task.Description = Console.ReadLine();
            task.CreationDate = DateTime.Now;
            task.State = Common.DTO.Enums.TaskStateDTO.Created;
            var res = await _crudService.CreateTask(task);
            Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            await BackToMenu();

        }

        private async Task UpdateTask()
        {
            ProjectTaskDTO task = new ProjectTaskDTO();
            Console.WriteLine("Insert ID");
            task.Id = CheckInput();
            Console.WriteLine("Insert Performer Id");
            task.PerformerId = CheckInput();
            Console.WriteLine("Insert Project Id");
            task.ProjectId = CheckInput();
            Console.WriteLine("Insert Name of Task");
            task.Name = Console.ReadLine();
            Console.WriteLine("Insert Description");
            task.Description = Console.ReadLine();
            task.CreationDate = DateTime.Now;
            task.State = Common.DTO.Enums.TaskStateDTO.Created;
            var res = await _crudService.UpdateTask(task);
            Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            await BackToMenu();

        }


        private async Task DeleteTask()
        {
            Console.WriteLine("Insert ID of Task");
            int id = CheckInput();
            try
            {
                var res = await _crudService.DeleteTask(id);
                Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            }
            catch (Exception)
            {
                Error("Not Found");
                throw;
            }
            await BackToMenu();
        }

        private async Task UsersCrudMenu()
        {
            Console.Clear();
            Console.WriteLine("Choose the action below for User");
            Console.WriteLine("1 - Get all Users");
            Console.WriteLine("2 - Get User by id");
            Console.WriteLine("3 - add User");
            Console.WriteLine("4 - update User");
            Console.WriteLine("5 - delete User");
            Console.WriteLine("0 - Exit");
            int value = CheckInput();
            switch (value)
            {
                case 1:
                    await PrintAllUsers();
                    break;
                case 2:
                    await PrintUser();
                    break;
                case 3:
                    await CreateUser();
                    break;
                case 4:
                    await UpdateUser();
                    break;
                case 5:
                    await DeleteUser();
                    break;
                case 0:
                    await UsersCrudMenu();
                    break;
                default:
                    await MainMenu();
                    return;
            }
        }

        private async Task PrintAllUsers()
        {
            var res = await _crudService.GetAllUsers();
            await PrintCollection(res);
        }

        private async Task PrintUser()
        {
            Console.WriteLine("Insert ID of User");
            int id = CheckInput();
            try
            {
                var res = await _crudService.GetUser(id);
                Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            }
            catch (Exception)
            {
                Error("Not Found");
                throw;
            }
            await BackToMenu();
        }

        private async Task CreateUser()
        {
            UserDTO user = new UserDTO();
            Console.WriteLine("Insert FirstName");
            user.FirstName = Console.ReadLine();
            Console.WriteLine("Insert Last Name");
            user.LastName = Console.ReadLine();
            Console.WriteLine("Insert Name of Task");
            user.TeamId = CheckInput();
            Console.WriteLine("Insert Description");
            user.RegisterationDate = DateTime.Now;
            var res = await _crudService.CreateUser(user);
            Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            await BackToMenu();

        }

        private async Task UpdateUser()
        {
            UserDTO user = new UserDTO();
            Console.WriteLine("Insert Id");
            user.Id = CheckInput();
            Console.WriteLine("Insert FirstName");
            user.FirstName = Console.ReadLine();
            Console.WriteLine("Insert Last Name");
            user.LastName = Console.ReadLine();
            Console.WriteLine("Insert Name of Task");
            user.TeamId = CheckInput();
            Console.WriteLine("Insert Description");
            user.RegisterationDate = DateTime.Now;
            var res = await _crudService.UpdateUser(user);
            Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            await BackToMenu();

        }

        private async Task DeleteUser()
        {
            Console.WriteLine("Insert ID of User");
            int id = CheckInput();
            try
            {
                var res = await _crudService.DeleteUser(id);
                Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            }
            catch (Exception)
            {
                Error("Not Found");
                throw;
            }
            await BackToMenu();
        }

        private async Task TeamsCrudMenu()
        {
            Console.Clear();
            Console.WriteLine("Choose the action below for Team");
            Console.WriteLine("1 - Get all Teams");
            Console.WriteLine("2 - Get Team by id");
            Console.WriteLine("3 - add Team");
            Console.WriteLine("4 - update Team");
            Console.WriteLine("5 - delete Team");
            int value = CheckInput();
            switch (value)
            {
                case 1:
                    await PrintAllTeams();
                    break;
                case 2:
                    await PrintTeam();
                    break;
                case 3:
                    await CreateTeam();
                    break;
                case 4:
                    await UpdateTeam();
                    break;
                case 5:
                    await DeleteTeam();
                    break;
                case 0:
                    await TeamsCrudMenu();
                    break;
                default:
                    await MainMenu();
                    return;
            }
        }

        private async Task PrintAllTeams()
        {
            var res = await _crudService.GetAllTeams();
            await PrintCollection(res);
            
        }

        private async Task PrintTeam()
        {
            Console.WriteLine("Insert ID of Team");
            int id = CheckInput();
            try
            {
                var res = await _crudService.GetTeam(id);
                Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            }
            catch (Exception)
            {
                Error("Not Found");
                throw;
            }
            await BackToMenu();
        }

        private async Task CreateTeam()
        {
            TeamDTO team = new TeamDTO();
            Console.WriteLine("Insert Name");
            team.Name = Console.ReadLine();
            team.CreatedAt= DateTime.Now;
            var res = await _crudService.CreateTeam(team);
            Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            await BackToMenu();

        }

        private async Task  UpdateTeam()
        {
            TeamDTO team = new TeamDTO();
            Console.WriteLine("Insert ID");
            team.Id = CheckInput();
            Console.WriteLine("Insert Name");
            team.Name = Console.ReadLine();
            team.CreatedAt = DateTime.Now;
            var res = await _crudService.UpdateTeam(team);
            Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            await BackToMenu();

        }

        private async Task DeleteTeam()
        {
            Console.WriteLine("Insert ID of Team");
            int id = CheckInput();
            try
            {
                var res = await _crudService.DeleteTeam(id);
                Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
            }
            catch (Exception)
            {
                Error("Not Found");
                throw;
            }
            await BackToMenu();
        }
        #endregion
        private async Task QueryMenu()
        {
            Console.Clear();
            Console.WriteLine("Welcome to Query Menu. Choose the action below:");
            Console.WriteLine("1 - Get Dictionary of User tasks in each User project (by id)");
            Console.WriteLine("2 - List of tasks for concrete User (by id)");
            Console.WriteLine("3 - Get list (id, name) of finished tasks in 2020 for concrete user (by id)");
            Console.WriteLine("4 - Get list (id, team name and users) of users older 10 sorted by registration date");
            Console.WriteLine("5 - Get list of all users with sorted tasks");
            Console.WriteLine("6 - Get User data structure");
            Console.WriteLine("7 - Get Project data structure");
            Console.WriteLine("0 - Exit");
            int value = CheckInput();
            switch (value)
            {
                case 1:
                    await PrintDictionary();
                    break;
                case 2:
                    await PrintListOfTasks();
                    break;
                case 3:
                    await PrintListOfFinishedTasks();
                    break;
                case 4:
                    await PrintTeamsWithUsers();
                    break;
                case 5:
                    await PrintUsersByNameAndTasks();
                    break;
                case 6:
                    await PrintUserInfo();
                    break;
                case 7:
                    await PrintProjectsInfo();
                    break;
                case 0:
                    await MainMenu();
                    return;
                default:
                    await MainMenu();
                    break;
            }
        }

        #endregion




        #region Query Methods
        private async Task PrintDictionary()
        {
            Warning("Insert ID");
            int id = CheckInput();
            Dictionary<ProjectDTO, int> res = await _queryService.GetDictionaryOfUserProjects(id);
            await EmptyCheck(res);
            foreach (var item in res)
            {
                string project = JsonConvert.SerializeObject(item.Key, Formatting.Indented);
                Console.WriteLine(project);
                Warning("tasks amount : " + item.Value);
            }
            await BackToMenu();
            return;
        }

        private async Task PrintListOfTasks()
        {
            Warning("Insert ID");
            int id = CheckInput();
            var res = await _queryService.GetUserTasks(id);
            await EmptyCheck(res.ToList());
            foreach (var item in res)
            {
                Console.WriteLine(JsonConvert.SerializeObject(item, Formatting.Indented));
                Console.WriteLine();
            }
            await BackToMenu();
            return;
        }

        private async Task PrintListOfFinishedTasks()
        {
            Warning("Insert ID");
            int id = CheckInput();
            var res =  await _queryService.GetUserFinishedTasks(id);
            await EmptyCheck(res.ToList());
            foreach (var item in res)
            {
                Console.WriteLine(JsonConvert.SerializeObject(item, Formatting.Indented));
                Console.WriteLine();
            }
            await BackToMenu();
            return;
        }

        private async Task PrintTeamsWithUsers()
        {
            var res = await _queryService.GetTeamsWithOldUsers();
            await EmptyCheck(res.ToList());
            foreach (var item in res)
            {
                Console.WriteLine(JsonConvert.SerializeObject(item, Formatting.Indented));
                Console.WriteLine();
            }
            await BackToMenu();
            return;
        }


        private async Task PrintUsersByNameAndTasks()
        {
            var res = await _queryService.GetUsersWithTasks();
            await EmptyCheck(res.ToList());
            foreach (var item in res)
            {
                Console.WriteLine(JsonConvert.SerializeObject(item, Formatting.Indented));
                Console.WriteLine();
            }
            await BackToMenu();
            return;
        }

        private async Task PrintUserInfo()
        {
            Warning("Insert ID");
            int id = CheckInput();
            try
            {
                UserInfoModel res = await _queryService.GetUserInfoModel(id);
                Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));

                await BackToMenu();
            }
            catch (Exception)
            {
                Error("This User don't have any projects");

                await BackToMenu();
                return;
            }
        }

        private async Task PrintProjectsInfo()
        {
            var res = await _queryService.GetProjectsInfo();
            await EmptyCheck(res.ToList());
            foreach (var item in res)
            {
                Warning("ID: " + item.Project.Id);
                Console.WriteLine(JsonConvert.SerializeObject(item, Formatting.Indented));
                Console.WriteLine();
                Console.WriteLine();
            }

            await BackToMenu();
            return;
        }
        #endregion

        #region Helpers
        private async Task PrintCollection<T>(T collection) where T: IEnumerable
        {
            foreach (var item in collection)
            {
                string value = JsonConvert.SerializeObject(item, Formatting.Indented);
                Console.WriteLine(value);
            }
            await BackToMenu();
        }
        private int CheckInput()
        {
            string input = Console.ReadLine();
            int result;
            if (!int.TryParse(input, out result))
            {
                Error("Insert int value");
                return CheckInput();
            }
            return result;
        }

        private async Task EmptyCheck<T>(T collection) where T : ICollection
        {
            if (collection.Count.Equals(0))
            {
                Console.WriteLine("Collection is Empty");
                Console.ReadKey();
                await MainMenu();
                return;
            }
        }

        private async Task  BackToMenu()
        {
            Console.ReadKey();
            await MainMenu();
            return;
        }

        internal static void Warning(string text)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }

        internal static void Error(string text)
        {
            Console.Beep();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }

        internal static void Success(string text)
        {
            Console.Beep();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }
        #endregion
    }
}
