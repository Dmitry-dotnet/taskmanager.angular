﻿using System.Threading.Tasks;

namespace LinqBinary.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var menu = new Menu();
            await menu.MainMenu();
           
        }
    }
}
