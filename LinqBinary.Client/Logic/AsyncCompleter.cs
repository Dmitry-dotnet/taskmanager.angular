﻿using LinqBinary.Common.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Timers;
using System.Threading.Tasks;

namespace LinqBinary.Client.Logic
{
    public class AsyncCompleter
    {
        private readonly HttpClient _client;
        private const string url = "http://localhost:64256/api/tasks/";
        Timer _timer;
        public AsyncCompleter()
        {
            _client = new HttpClient();
            _timer = new Timer();
        }

        

        public async Task CompleteAsync()
        {
            var res = await _client.GetStringAsync(url);
            List<ProjectTaskDTO> tasks = JsonConvert.DeserializeObject<List<ProjectTaskDTO>>(res);
            if (tasks.Any(x => x.State != Common.DTO.Enums.TaskStateDTO.Finished))
            {
                tasks = tasks.Where(x => x.State != Common.DTO.Enums.TaskStateDTO.Finished).ToList();
                var rnd = new Random();
                var task = tasks[rnd.Next(tasks.Count)];
                task.State = Common.DTO.Enums.TaskStateDTO.Finished;

                string jsonString = JsonConvert.SerializeObject(task);
                var postResponce = await _client.PutAsync(url
                    , new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var updatedTask = JsonConvert.DeserializeObject<ProjectTaskDTO>(await postResponce.Content.ReadAsStringAsync());

                Menu.Success($"Task with id {updatedTask.Id} just finished");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("All tasks completed");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
