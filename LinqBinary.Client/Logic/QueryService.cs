﻿using LinqBinary.Common.DTO;
using LinqBinary.Common.DTO.DataModels;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace LinqBinary.Client.Logic
{
    public class QueryService
    {
        private readonly HttpClient _httpClient;

        public QueryService()
        {
            _httpClient = new HttpClient();
        }

        //1
        public async Task<Dictionary<ProjectDTO, int>> GetDictionaryOfUserProjects(int id)
        {
            string res = await _httpClient.GetStringAsync($"http://localhost:64256/api/special/dictionary/{id}");

            return JsonConvert.DeserializeObject<Dictionary<ProjectDTO, int>>(res);
        }

        //2
        public async Task<IEnumerable<ProjectTaskDTO>> GetUserTasks(int id)
        {
            string res = await _httpClient.GetStringAsync($"http://localhost:64256/api/special/userTasks/{id}");

            return JsonConvert.DeserializeObject<IEnumerable<ProjectTaskDTO>>(res);
        }

        //3
        public async Task<IEnumerable<FinishedTaskModel>> GetUserFinishedTasks(int id)
        {
            string res = await _httpClient.GetStringAsync($"http://localhost:64256/api/special/finishedTasks/{id}");
            return JsonConvert.DeserializeObject<IEnumerable<FinishedTaskModel>>(res);
        }

        //4
        public async Task<IEnumerable<TeamWithUserModel>> GetTeamsWithOldUsers()
        {
            string res = await _httpClient.GetStringAsync("http://localhost:64256/api/special/oldUsersTeam");
            return JsonConvert.DeserializeObject<IEnumerable<TeamWithUserModel>>(res);
        }

        //5
        public async Task<IEnumerable<UserTasksModel>> GetUsersWithTasks()
        {
            string res = await _httpClient.GetStringAsync("http://localhost:64256/api/special/usersWithTasks");
            return JsonConvert.DeserializeObject<IEnumerable<UserTasksModel>>(res);
        }

        //6
        public async Task<UserInfoModel> GetUserInfoModel(int id)
        {
            string res = await _httpClient.GetStringAsync($"http://localhost:64256/api/special/userInfo/{id}");
            return JsonConvert.DeserializeObject<UserInfoModel>(res);
        }

        //7
        public async Task<IEnumerable<ProjectInfoModel>> GetProjectsInfo()
        {
            string res = await _httpClient.GetStringAsync($"http://localhost:64256/api/special/projectsInfo/");
            return JsonConvert.DeserializeObject<IEnumerable<ProjectInfoModel>>(res);
        }

    }
}
