﻿using LinqBinary.Common.DTO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LinqBinary.Client.Logic
{
    public class CrudService
    {
        private readonly HttpClient _httpClient;
        public CrudService()
        {
            _httpClient = new HttpClient();
        }

        #region Project
        public async Task<List<ProjectDTO>> GetAllProjects()
        {
            var res = await _httpClient.GetStringAsync("http://localhost:64256/api/projects/");
            List<ProjectDTO> result = JsonConvert.DeserializeObject<List<ProjectDTO>>(res);
            return result;
        }

        public async Task<ProjectDTO> GetProject(int id)
        {
            var res = await _httpClient.GetStringAsync($"http://localhost:64256/api/projects/{id}");
            ProjectDTO result = JsonConvert.DeserializeObject<ProjectDTO>(res);
            return result;
        }

        public async Task<ProjectDTO> CreateProject(ProjectDTO project)
        {
            var dataJson = JsonConvert.SerializeObject(project);
            var data = new StringContent(dataJson, Encoding.UTF8, "application/json");
            var res = await _httpClient.PostAsync($"http://localhost:64256/api/projects/", data);
            return JsonConvert.DeserializeObject<ProjectDTO>(await res.Content.ReadAsStringAsync());

        }

        public async Task<ProjectDTO> UpdateProject(ProjectDTO project)
        {
            var dataJson = JsonConvert.SerializeObject(project);
            var data = new StringContent(dataJson, Encoding.UTF8, "application/json");
            var res = await _httpClient.PutAsync($"http://localhost:64256/api/projects/", data);
            return JsonConvert.DeserializeObject<ProjectDTO>(await res.Content.ReadAsStringAsync());
        }

        public async Task<string> DeleteProject(int id)
        {
            var res = await _httpClient.DeleteAsync($"http://localhost:64256/api/projects/{id}");
            return res.StatusCode.ToString();
        }
        #endregion

        #region Task
        public async Task<List<ProjectTaskDTO>> GetAllTasks()
        {
            var res = await _httpClient.GetStringAsync("http://localhost:64256/api/tasks/");
            List<ProjectTaskDTO> result = JsonConvert.DeserializeObject<List<ProjectTaskDTO>>(res);
            return result;
        }

        public async Task<ProjectTaskDTO> GetTask(int id)
        {
            var res = await _httpClient.GetStringAsync($"http://localhost:64256/api/tasks/{id}");
            ProjectTaskDTO result = JsonConvert.DeserializeObject<ProjectTaskDTO>(res);
            return result;
        }

        public async Task<ProjectTaskDTO> CreateTask(ProjectTaskDTO project)
        {
            var dataJson = JsonConvert.SerializeObject(project);
            var data = new StringContent(dataJson, Encoding.UTF8, "application/json");
            var res = await _httpClient.PostAsync($"http://localhost:64256/api/tasks/", data);
            return JsonConvert.DeserializeObject<ProjectTaskDTO>(await res.Content.ReadAsStringAsync());

        }

        public async Task<ProjectTaskDTO> UpdateTask(ProjectTaskDTO project)
        {
            var dataJson = JsonConvert.SerializeObject(project);
            var data = new StringContent(dataJson, Encoding.UTF8,"application/json");
            var res = await _httpClient.PutAsync($"http://localhost:64256/api/tasks/", data);
            return JsonConvert.DeserializeObject<ProjectTaskDTO>(await res.Content.ReadAsStringAsync());
        }

        public async Task<string> DeleteTask(int id)
        {
            var res = await _httpClient.DeleteAsync($"http://localhost:64256/api/tasks/{id}");
            return res.StatusCode.ToString();
        }
        #endregion

        #region User
        public async Task<List<UserDTO>> GetAllUsers()
        {
            var res = await _httpClient.GetStringAsync("http://localhost:64256/api/Users/");
            List<UserDTO> result = JsonConvert.DeserializeObject<List<UserDTO>>(res);
            return result;
        }

        public async Task<UserDTO> GetUser(int id)
        {
            var res = await _httpClient.GetStringAsync($"http://localhost:64256/api/Users/{id}");
            UserDTO result = JsonConvert.DeserializeObject<UserDTO>(res);
            return result;
        }

        public async Task<UserDTO> CreateUser(UserDTO project)
        {
            var dataJson = JsonConvert.SerializeObject(project);
            var data = new StringContent(dataJson, Encoding.UTF8, "application/json");
            var res = await _httpClient.PostAsync($"http://localhost:64256/api/Users/", data);
            return JsonConvert.DeserializeObject<UserDTO>(await res.Content.ReadAsStringAsync());

        }

        public async Task<UserDTO> UpdateUser(UserDTO project)
        {
            var dataJson = JsonConvert.SerializeObject(project);
            var data = new StringContent(dataJson, Encoding.UTF8, "application/json");
            var res = await _httpClient.PutAsync($"http://localhost:64256/api/Users/", data);
            return JsonConvert.DeserializeObject<UserDTO>(await res.Content.ReadAsStringAsync());
        }

        public async Task<string> DeleteUser(int id)
        {
            var res = await _httpClient.DeleteAsync($"http://localhost:64256/api/Users/{id}");
            return res.StatusCode.ToString();
        }
        #endregion

        #region Team
        public async Task<List<TeamDTO>> GetAllTeams()
        {
            var res = await _httpClient.GetStringAsync("http://localhost:64256/api/Teams/");
            List<TeamDTO> result = JsonConvert.DeserializeObject<List<TeamDTO>>(res);
            return result;
        }

        public async Task<TeamDTO> GetTeam(int id)
        {
            var res = await _httpClient.GetStringAsync($"http://localhost:64256/api/Teams/{id}");
            TeamDTO result = JsonConvert.DeserializeObject<TeamDTO>(res);
            return result;
        }

        public async Task<TeamDTO> CreateTeam(TeamDTO project)
        {
            var dataJson = JsonConvert.SerializeObject(project);
            var data = new StringContent(dataJson, Encoding.UTF8, "application/json");
            var res = await _httpClient.PostAsync($"http://localhost:64256/api/Teams/", data);
            return JsonConvert.DeserializeObject<TeamDTO>(await res.Content.ReadAsStringAsync());

        }

        public async Task<TeamDTO> UpdateTeam(TeamDTO project)
        {
            var dataJson = JsonConvert.SerializeObject(project);
            var data = new StringContent(dataJson, Encoding.UTF8, "application/json");
            var res = await _httpClient.PutAsync($"http://localhost:64256/api/Teams/", data);
            return JsonConvert.DeserializeObject<TeamDTO>(await res.Content.ReadAsStringAsync());
        }

        public async Task<string> DeleteTeam(int id)
        {
            var res = await _httpClient.DeleteAsync($"http://localhost:64256/api/Teams/{id}");
            return res.StatusCode.ToString();
        }
        #endregion
    }
}
