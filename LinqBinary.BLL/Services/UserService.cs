﻿using AutoMapper;
using LinqBinary.BLL.Services.Interfaces;
using LinqBinary.Common.DTO;
using LinqBinary.DAL.Context;
using LinqBinary.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinqBinary.BLL.Services
{
    public class UserService : IEntityService<UserDTO>
    {
        LinqBinaryDbContext _context;
        IMapper _mapper;

        public UserService(LinqBinaryDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<UserDTO> Create(UserDTO t)
        {
            try
            {
                var entity = _mapper.Map<User>(t);
                entity.RegisterationDate = DateTime.Now;
                _context.Users.Add(entity);
                await _context.SaveChangesAsync();
                return _mapper.Map<UserDTO>(entity);


            }
            catch (Exception)
            {
                throw new ArgumentException();
            }
        }

        public async Task Delete(int id)
        {
            var entity = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (entity != null)
            {
                _context.Users.Remove(entity);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public async Task<UserDTO> Get(int id)
        {
            var any = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (any != null)
            {
                return _mapper.Map<UserDTO>(any);
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public async Task<IEnumerable<UserDTO>> GetAll()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(await _context.Users.ToListAsync());
        }

        public async Task<UserDTO> Update(UserDTO t)
        {
            var entity = _mapper.Map<User>(t);
            var existing = _context.Users.FirstOrDefault(x => x.Id == entity.Id);
            if (existing != null)
            {
                _context.Entry(existing).CurrentValues.SetValues(entity);
                await _context.SaveChangesAsync();
                return _mapper.Map<UserDTO>(existing);
            }
            else
            {
                throw new NullReferenceException();
            }
        }
    }
}
