﻿using AutoMapper;
using LinqBinary.BLL.Services.Interfaces;
using LinqBinary.Common.DTO;
using LinqBinary.DAL.Context;
using LinqBinary.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinqBinary.BLL.Services
{
    public class TeamService : IEntityService<TeamDTO>
    {
        LinqBinaryDbContext _context;
        IMapper _mapper;

        public TeamService(LinqBinaryDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<TeamDTO> Create(TeamDTO t)
        {
            var entity = _mapper.Map<Team>(t);
            entity.CreatedAt = DateTime.Now;
            _context.Teams.Add(entity);
            await _context.SaveChangesAsync();
            return _mapper.Map<TeamDTO>(entity);
        }

        public async Task Delete(int id)
        {
            var entity = await _context.Teams.FirstOrDefaultAsync(x => x.Id == id);
            if (entity != null)
            {
                _context.Teams.Remove(entity);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public async Task<TeamDTO> Get(int id)
        {
            var any = await _context.Teams.FirstOrDefaultAsync(x => x.Id == id);
            if (any != null)
            {
                return _mapper.Map<TeamDTO>(any);
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public async Task<IEnumerable<TeamDTO>> GetAll()
        {
            return _mapper.Map<IEnumerable<TeamDTO>>(await _context.Teams.ToListAsync());
        }

        public async Task<TeamDTO> Update(TeamDTO t)
        {
            var entity = _mapper.Map<Team>(t);
            var existing = _context.Teams.FirstOrDefault(x => x.Id == entity.Id);
            if (existing != null)
            {
                _context.Entry(existing).CurrentValues.SetValues(entity);
                await _context.SaveChangesAsync();
                return _mapper.Map<TeamDTO>(existing);
            }
            else
            {
                throw new NullReferenceException();
            }
        }
    }
}
