﻿using AutoMapper;
using LinqBinary.BLL.Services.Interfaces;
using LinqBinary.BLL.Services.SpecialQuery.Interfaces;
using LinqBinary.Common.DTO;
using LinqBinary.Common.DTO.DataModels;
using LinqBinary.Common.DTO.Enums;
using LinqBinary.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinqBinary.BLL.Services.SpecialQuery
{
    public class SpecialQueryService : ISpecialQueryService
    {
        IEnumerable<ProjectDTO> _projects;
        IEnumerable<TeamDTO> _teams;
        IEnumerable<UserDTO> _users;
        IEnumerable<ProjectTaskDTO> _projectTasks;
        

        public SpecialQueryService(LinqBinaryDbContext context, IMapper mapper)
        {

            _projects = mapper.Map<List<ProjectDTO>>(context.Projects);
            _teams = mapper.Map<List<TeamDTO>>(context.Teams);
            _users = mapper.Map<List<UserDTO>>(context.Users);
            _projectTasks = mapper.Map<List<ProjectTaskDTO>>(context.ProjectTasks);
        }

        //1
        public async Task<Dictionary<ProjectDTO, int>> GetDictionaryOfUserProjects(int id)
        {
            var res = await Task.FromResult(_projects
               .Where(x => x.AuthorId == id)
               .ToDictionary(p => p, t => _projectTasks
               .Count(x => x.ProjectId == t.Id)));
            return res;
        }

        //2
        public async Task<IEnumerable<ProjectTaskDTO>> GetUsersTasks(int id)
        {
            var res = await Task.FromResult(_projectTasks
                .Where(x => x.PerformerId == id && x.Name.Length < 45));

            return res;
        }

        //3
        public async Task<IEnumerable<FinishedTaskModel>> GetUserFinishedTasks(int id)
        {
            var res = await Task.FromResult(_projectTasks.Where(x => x.PerformerId == id
                            && x.State == TaskStateDTO.Finished
                            && x.FinishedAt.Year == 2020)
                .Select(t => new FinishedTaskModel { Id = t.Id, Name = t.Name }));

            return res;
        }

        //4
        public async Task<IEnumerable<TeamWithUserModel>> GetTeamsWithOldUsers()
        {
            var res = await  Task.FromResult(_teams
                .GroupJoin(_users, 
                 x => x.Id, y => y.TeamId,
                (x, y) => new TeamWithUserModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Users = y
                .OrderByDescending(d => d.RegisterationDate).ToList()
                }).Where(x => !x.Users.Any(y => y.BirthDayDate.Year > 2010) && x.Users.Count > 0));
            
            return res;
        }

        //5
        public async Task<IEnumerable<UserTasksModel>> GetUsersWithTasks()
        {
            var res = await Task.FromResult(_users.OrderBy(n => n.FirstName)
                .GroupJoin(_projectTasks,
                x => x.Id,
                y => y.PerformerId,
                (x, y) => new UserTasksModel
                {
                    User = x,
                    Tasks = y
                    .OrderByDescending(n => n.Name.Length).ToList()
                }));

            return res;
        }

        //6
        public async Task<UserInfoModel> GetUserInfoModel(int id)
        {
            UserInfoModel res =await Task.FromResult(_users.Where(i => i.Id == id).
                     GroupJoin(_projects, x => x.Id, y => y.AuthorId,
                     (user, lastProject) => new
                     {
                         User = user,
                         LastProject = lastProject.OrderByDescending(x => x.CreationDate).First()
                     })
                     //At first, I join all tasks of project
                     .GroupJoin(_projectTasks, x => x.LastProject.Id, y => y.ProjectId,
                     (x, y) => new
                     {
                         x.User,
                         x.LastProject,
                         ProjectTasks = y.Count(),
                     })
                     //And after that I join tasks of user and creating a model
                     .GroupJoin(_projectTasks, x => x.User.Id, y => y.PerformerId,
                     (x, y) => new UserInfoModel
                     {
                         User = x.User,
                         LastProject = x.LastProject,
                         ProjectTasks = x.ProjectTasks,
                         CanceledOrUnFinishedTasks = y.Count(t =>
                                                     (t.State == TaskStateDTO.Canceled
                                                     || t.State == TaskStateDTO.Started)),
                         LongestTask = y.OrderByDescending(x => x.FinishedAt.Subtract(x.CreationDate))
                                        .FirstOrDefault()

                     }).FirstOrDefault());

            return res;

        }

        //7
        public async Task<IEnumerable<ProjectInfoModel>> GetProjectsInfo()
        {
            var res = await Task.FromResult(_projects.GroupJoin(_projectTasks, x => x.Id, y => y.ProjectId,
                (x, y) => new
                {
                    Project = x,
                    TaskWithLongestDescr = y.OrderByDescending(x => x.Description.Length).FirstOrDefault(),
                    TaskWithShortestName = y.OrderBy(x => x.Name.Length).FirstOrDefault()
                })
                .GroupJoin(_users, x => x.Project.TeamId, y => y.TeamId,
                (x, y) => new ProjectInfoModel
                {
                    Project = x.Project,
                    TaskWithLongestDescr = x.TaskWithLongestDescr,
                    TaskWithShortestName = x.TaskWithShortestName,
                    UsersInTeam = (x.Project.Description.Length > 20
                    || _projectTasks.Where(a => a.ProjectId == x.Project.Id).Count() < 3) ? y.Count() : 0
                }));

            return res;
        }



        //8 (EXTRA FOR TESTS)
        public async Task<UserUnfinishedTasksModel> UserUnfinishedTasks(int id)
        {
            var res = await Task.FromResult(_users.Where(x => x.Id == id)
                .GroupJoin(_projectTasks, x => x.Id, y => y.PerformerId,
                (x, y) => new UserUnfinishedTasksModel
                {
                    User = x,
                    Tasks = y.Where(s => s.State != TaskStateDTO.Finished).ToList()
                }).FirstOrDefault());
            if (res == null)
            {
                throw new NullReferenceException();
            }
            return res;
        } 
    }
}
