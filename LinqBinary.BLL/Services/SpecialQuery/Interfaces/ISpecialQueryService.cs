﻿using LinqBinary.Common.DTO;
using LinqBinary.Common.DTO.DataModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LinqBinary.BLL.Services.SpecialQuery.Interfaces
{
    public interface ISpecialQueryService
    {
        Task<Dictionary<ProjectDTO, int>> GetDictionaryOfUserProjects(int id);
        Task<IEnumerable<ProjectTaskDTO>> GetUsersTasks(int id);
        Task<IEnumerable<FinishedTaskModel>> GetUserFinishedTasks(int id);
        Task <IEnumerable<TeamWithUserModel>> GetTeamsWithOldUsers();
        Task <IEnumerable<UserTasksModel>> GetUsersWithTasks();
        Task<UserInfoModel >GetUserInfoModel(int id);
        Task <IEnumerable<ProjectInfoModel>> GetProjectsInfo();
        Task <UserUnfinishedTasksModel> UserUnfinishedTasks(int id);
    }
}
