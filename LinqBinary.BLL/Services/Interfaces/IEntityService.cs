﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LinqBinary.BLL.Services.Interfaces
{
    public interface IEntityService <T>
    {
        Task<IEnumerable<T>> GetAll();

        Task<T> Get(int id);
        Task<T> Create(T t);
        Task<T> Update(T t);
        Task Delete(int id);
    }
}
