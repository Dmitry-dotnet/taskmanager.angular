﻿using AutoMapper;
using LinqBinary.BLL.Services.Interfaces;
using LinqBinary.Common.DTO;
using LinqBinary.DAL.Context;
using LinqBinary.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinqBinary.BLL.Services
{
    public class ProjectService : IEntityService<ProjectDTO>
    {
        LinqBinaryDbContext _context;
        IMapper _mapper;

        public ProjectService(LinqBinaryDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ProjectDTO> Create(ProjectDTO t)
        {
            var entity = _mapper.Map<Project>(t);
            entity.CreationDate = DateTime.Now;
            _context.Projects.Add(entity);
            await _context.SaveChangesAsync();
            return _mapper.Map<ProjectDTO>(entity);
        }

        public async Task Delete(int id)
        {
            var entity = await _context.Projects.FirstOrDefaultAsync(x => x.Id == id);
            if (entity != null)
            {
                _context.Projects.Remove(entity);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public async Task<ProjectDTO> Get(int id)
        {
            var any = await _context.Projects.FirstOrDefaultAsync(x => x.Id == id);
            if (any != null)
            {
                return _mapper.Map<ProjectDTO>(any);
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public async Task<IEnumerable<ProjectDTO>> GetAll()
        {
            return _mapper.Map<IEnumerable<ProjectDTO>>(await _context.Projects.ToListAsync());
        }

        public async Task<ProjectDTO> Update (ProjectDTO t)
        {
            var entity = _mapper.Map<Project>(t);
            var existing = _context.Projects.FirstOrDefault(x => x.Id == entity.Id);
            if (existing != null)
            {
                _context.Entry(existing).CurrentValues.SetValues(entity);
                await _context.SaveChangesAsync();
                return _mapper.Map<ProjectDTO>(existing);
            }
            else
            {
                throw new NullReferenceException();
            }
        }
    }
}
