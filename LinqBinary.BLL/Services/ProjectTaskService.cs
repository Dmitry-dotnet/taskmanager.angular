﻿using AutoMapper;
using LinqBinary.BLL.Services.Interfaces;
using LinqBinary.Common.DTO;
using LinqBinary.DAL.Context;
using LinqBinary.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinqBinary.BLL.Services
{
    public class ProjectTaskService : IEntityService<ProjectTaskDTO>
    {
        LinqBinaryDbContext _context;
        IMapper _mapper;

        public ProjectTaskService(LinqBinaryDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ProjectTaskDTO> Create(ProjectTaskDTO t)
        {
            var entity = _mapper.Map<ProjectTask>(t);
            entity.CreationDate = DateTime.Now;
            _context.ProjectTasks.Add(entity);
            await _context.SaveChangesAsync();
            return _mapper.Map<ProjectTaskDTO>(entity);
        }

        public async Task Delete(int id)
        {
            var entity = await _context.ProjectTasks.FirstOrDefaultAsync(x => x.Id == id);
            if (entity != null)
            {
                _context.ProjectTasks.Remove(entity);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public async Task<ProjectTaskDTO> Get(int id)
        {
            var any = await _context.ProjectTasks.FirstOrDefaultAsync(x => x.Id == id);
            if (any != null)
            {
                return _mapper.Map<ProjectTaskDTO>(any);
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public async Task<IEnumerable<ProjectTaskDTO>> GetAll()
        {
            return _mapper.Map<IEnumerable<ProjectTaskDTO>>(await _context.ProjectTasks.ToListAsync());
        }

        public async Task<ProjectTaskDTO> Update(ProjectTaskDTO t)
        {
            var entity = _mapper.Map<ProjectTask>(t);
            var existing = _context.ProjectTasks.FirstOrDefault(x => x.Id == entity.Id);
            if (existing != null)
            {
                _context.Entry(existing).CurrentValues.SetValues(entity);
                await _context.SaveChangesAsync();
                return _mapper.Map<ProjectTaskDTO>(existing);
            }
            else
            {
                throw new NullReferenceException();
            }
        }
    }
}
