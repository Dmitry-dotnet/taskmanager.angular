﻿using AutoMapper;
using LinqBinary.Common.DTO;
using LinqBinary.DAL.Entities;

namespace LinqBinary.BLL.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();
        }
    }
}
