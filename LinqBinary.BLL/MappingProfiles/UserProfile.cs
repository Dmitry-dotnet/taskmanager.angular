﻿using AutoMapper;
using LinqBinary.Common.DTO;
using LinqBinary.DAL.Entities;

namespace LinqBinary.BLL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>().ForMember(m => m.Team, src => src.Ignore());
        }
    }
}
