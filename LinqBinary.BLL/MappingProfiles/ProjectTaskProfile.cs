﻿using AutoMapper;
using LinqBinary.Common.DTO;
using LinqBinary.DAL.Entities;

namespace LinqBinary.BLL.MappingProfiles
{
    public class ProjectTaskProfile : Profile
    {
        public ProjectTaskProfile()
        {
            CreateMap<ProjectTask, ProjectTaskDTO>().ForMember(m => m.State, src => src.MapFrom(dest => dest.State));
            CreateMap<ProjectTaskDTO, ProjectTask>().ForMember(m => m.State, src => src.MapFrom(dest => dest.State))
                .ForMember(m => m.Performer, src => src.Ignore())
                .ForMember(m => m.Project, src => src.Ignore());
        }
    }
}
