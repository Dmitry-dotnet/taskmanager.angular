﻿using AutoMapper;
using LinqBinary.Common.DTO;
using LinqBinary.DAL.Entities;

namespace LinqBinary.BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, Project>().ForMember(m => m.Team, src => src.Ignore())
                                            .ForMember(m=> m.Author, src => src.Ignore());

        }
    }
}
