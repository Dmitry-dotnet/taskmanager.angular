﻿using LinqBinary.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LinqBinary.DAL.Context
{
    public class LinqBinaryDbContext : DbContext
    {
        public LinqBinaryDbContext(DbContextOptions<LinqBinaryDbContext> options )
            :base(options)
        {

        }

        

        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectTask> ProjectTasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().Property(p => p.FirstName).IsRequired().HasMaxLength(25);
            modelBuilder.Entity<User>().Property(p => p.LastName).IsRequired().HasMaxLength(25);
            modelBuilder.Entity<User>().Property(p => p.RegisterationDate).HasDefaultValue(DateTime.Now);
            modelBuilder.Entity<User>().Property(p => p.TeamId).HasDefaultValue(null);
            modelBuilder.Entity<Project>().Property(p => p.Name).IsRequired();
            modelBuilder.Entity<Project>().Property(p => p.CreationDate).HasDefaultValue(DateTime.Now);
            modelBuilder.Entity<Project>().Property(p => p.Description).HasDefaultValue("No description Yet");
            modelBuilder.Entity<Project>().Property(p => p.AuthorId).HasDefaultValue(null);
            modelBuilder.Entity<Project>().Property(p => p.TeamId).HasDefaultValue(null);
            modelBuilder.Entity<Team>().Property(p => p.CreatedAt).HasDefaultValue(DateTime.Now);
            modelBuilder.Entity<Team>().Property(p => p.Name).IsRequired().HasMaxLength(50);
            modelBuilder.Entity<ProjectTask>().Property(p => p.Name).IsRequired();
            modelBuilder.Entity<ProjectTask>().Property(p => p.CreationDate).HasDefaultValue(DateTime.Now);
            modelBuilder.Entity<ProjectTask>().Property(p => p.PerformerId).HasDefaultValue(null);
            modelBuilder.Entity<ProjectTask>().Property(p => p.ProjectId).HasDefaultValue(null);



            base.OnModelCreating(modelBuilder);
        }

    }

}
