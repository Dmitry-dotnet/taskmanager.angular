﻿namespace LinqBinary.DAL.Enums
{
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
