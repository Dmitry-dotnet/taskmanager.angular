﻿using LinqBinary.DAL.Entities.Interfaces;
using LinqBinary.DAL.Enums;
using System;

namespace LinqBinary.DAL.Entities
{
    public class ProjectTask : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }
        public int? ProjectId { get; set; }
        public Project Project { get; set; }

        public int? PerformerId { get; set; }
        public User Performer { get; set; }
    }
}
