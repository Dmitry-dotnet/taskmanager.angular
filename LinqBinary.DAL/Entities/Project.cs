﻿using LinqBinary.DAL.Entities.Interfaces;
using System;

namespace LinqBinary.DAL.Entities
{
    public class Project : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime Deadline { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public int? AuthorId { get; set; }
        public User Author { get; set; }

    }
}
