﻿using LinqBinary.DAL.Entities.Interfaces;
using System;

namespace LinqBinary.DAL.Entities
{
    public class User : IEntity
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDayDate { get; set; }
        public DateTime RegisterationDate { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }

    }
}
