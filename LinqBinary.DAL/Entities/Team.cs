﻿using LinqBinary.DAL.Entities.Interfaces;
using System;

namespace LinqBinary.DAL.Entities
{
    public class Team : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
