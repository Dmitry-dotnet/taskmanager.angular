﻿using AutoMapper;
using LinqBinary.BLL.Services;
using LinqBinary.Common.DTO;
using LinqBinary.DAL.Context;
using LinqBinary.DAL.Entities;
using LinqBinary.Tests.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LinqBinary.Tests
{
    public class UserServiceTests
    {
        IMapper _mapper;
        UserService _service;
        public UserServiceTests()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserDTO>();
                cfg.CreateMap<UserDTO, User>().ForMember(m => m.Team, src => src.Ignore());

            });
            _mapper = config.CreateMapper();
        }

        [Fact]
        public async Task AddingUser_CanAdd()
        {
            using (var context = new LinqBinaryDbContext(
                ContextCleaner.CreateNewContextOptions()))
            {
                int amount = context.Users.Count();
                UserDTO user = new UserDTO
                {
                    Id = 1,
                    Email = "qweqwewq12312",
                    FirstName = "John",
                    LastName = "Bin",
                    BirthDayDate = DateTime.Now,
                    RegisterationDate = DateTime.Now
                };
                _service = new UserService(context, _mapper);
                await _service.Create(user);


                Assert.Single(context.Users);
            }
        }

        [Fact]
        public async Task AddUserToTeam()
        {
            using (var context = new LinqBinaryDbContext(
                ContextCleaner.CreateNewContextOptions()))
            {
                var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("Team.json"));
                var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("User.json"));
                UserDTO user = new UserDTO
                {   
                    Id = 51,
                    Email = "qweqwewq12312",
                    FirstName = "John",
                    LastName = "Bin"
                };
                context.Teams.AddRange(teams);
                context.Users.AddRange(users);
                context.SaveChanges();
                _service = new UserService(context, _mapper);
                await _service.Create(user);
                //count users in team 1
                int count = context.Users.Where(x => x.TeamId == 1).Count();

                user.TeamId = 1;
                user = await _service.Update(user);

                Assert.True(count < context.Users.Where(x => x.TeamId == 1).Count());
            }

        }

        
    }
}
