﻿using LinqBinary.DAL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinqBinary.Tests.Services
{
    internal static class ContextCleaner
    {
        internal static DbContextOptions<LinqBinaryDbContext> CreateNewContextOptions()
        {

            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            var builder = new DbContextOptionsBuilder<LinqBinaryDbContext>();
            builder.UseInMemoryDatabase(databaseName: "LinqBinaryDatabase")
                   .UseInternalServiceProvider(serviceProvider);

            return builder.Options;
        }
    }
}
