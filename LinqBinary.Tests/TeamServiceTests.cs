﻿using AutoMapper;
using LinqBinary.BLL.Services;
using LinqBinary.DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using LinqBinary.DAL.Entities;
using LinqBinary.Common.DTO;
using System.Threading.Tasks;

namespace LinqBinary.Tests
{
    public class TeamServiceTests
    {

        LinqBinaryDbContext _context;
        IMapper _mapper;
        
        TeamService _teamService;

        public TeamServiceTests()
        {
            var options = new DbContextOptionsBuilder<LinqBinaryDbContext>()
            .UseInMemoryDatabase(databaseName: "LinqBinaryDB")
            .Options;
            _context = new LinqBinaryDbContext(options);
            var config = new MapperConfiguration(cfg => 
            {
                cfg.CreateMap<Team, TeamDTO>();
                cfg.CreateMap<TeamDTO, Team>();
            });
            _mapper = config.CreateMapper();
            _teamService = new TeamService(_context, _mapper);

        }

        [Fact]
        public async Task TeamServiceGet_WhenNotExistingId_ThenThrowNullReferenceException()
        {
            var id = -1;

            await Assert.ThrowsAsync<NullReferenceException>(() => _teamService.Get(id));
        }
        [Fact]
        public async Task TeamService_CanAdd()
        {
            using (var context = _context)
            {
                var team = new TeamDTO()
                {
                    Id = 1,
                    CreatedAt = DateTime.Now,
                    Name = "TeamTest"
                };
                var result = await _teamService.Create(team);

                Assert.Equal(team.Id, result.Id);
                Assert.Equal(team.Name, result.Name);
                Assert.Equal(team.CreatedAt, result.CreatedAt);

                result = await  _teamService.Get(result.Id);
                Assert.Equal(team.Name, result.Name);

            }
        }

    }
}
