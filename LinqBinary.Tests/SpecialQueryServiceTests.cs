﻿using AutoMapper;
using FakeItEasy;
using LinqBinary.BLL.Services.SpecialQuery;
using LinqBinary.Common.DTO;
using LinqBinary.DAL.Context;
using LinqBinary.DAL.Entities;
using LinqBinary.Tests.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LinqBinary.Tests
{
    public class SpecialQueryServiceTests
    {
        IMapper _mapper;
        SpecialQueryService _service;
        public SpecialQueryServiceTests()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Team, TeamDTO>();
                cfg.CreateMap<TeamDTO, Team>();
                cfg.CreateMap<Project, ProjectDTO>();
                cfg.CreateMap<ProjectDTO, Project>().ForMember(m => m.Team, src => src.Ignore())
                                                .ForMember(m => m.Author, src => src.Ignore());
                cfg.CreateMap<ProjectTask, ProjectTaskDTO>().ForMember(m => m.State, src => src.MapFrom(dest => dest.State));
                cfg.CreateMap<ProjectTaskDTO, ProjectTask>().ForMember(m => m.State, src => src.MapFrom(dest => dest.State))
                    .ForMember(m => m.Performer, src => src.Ignore())
                    .ForMember(m => m.Project, src => src.Ignore());
                cfg.CreateMap<User, UserDTO>();
                cfg.CreateMap<UserDTO, User>().ForMember(m => m.Team, src => src.Ignore());

            });
            _mapper = config.CreateMapper();

        }


        [Fact]
        public async Task GetUnfinishedTasks()
        {
            using (var context = new LinqBinaryDbContext(ContextCleaner.CreateNewContextOptions()))
            {
                var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("User.json"));
                var tasks = JsonConvert.DeserializeObject<List<ProjectTask>>(File.ReadAllText("ProjectTask.json"));
                context.Users.AddRange(users);
                context.ProjectTasks.AddRange(tasks);
                context.SaveChanges();
                _service = new SpecialQueryService(context, _mapper);
                var res = await _service.UserUnfinishedTasks(1);


                Assert.Equal(2, res.Tasks.Count());
            }
        }

        [Fact]
        public async Task GetUnfinishedTasks_UserNotExist_ReturnEmpty()
        {
            using (var context = new LinqBinaryDbContext(ContextCleaner.CreateNewContextOptions()))
            {
               
                _service = new SpecialQueryService(context, _mapper);
                
               await Assert.ThrowsAsync<NullReferenceException>(() => _service.UserUnfinishedTasks(1));
            }
        }

        [Fact]
        public async Task GetDictionary_ReturnedDictionary()
        {
            using (var context = new LinqBinaryDbContext(ContextCleaner.CreateNewContextOptions()))
            {   //seed
                var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("Team.json"));
                var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("User.json"));
                var projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("Project.json"));
                var tasks = JsonConvert.DeserializeObject<List<ProjectTask>>(File.ReadAllText("ProjectTask.json"));

                context.Teams.AddRange(teams);
                context.Users.AddRange(users);
                context.Projects.AddRange(projects);
                context.ProjectTasks.AddRange(tasks);
                context.SaveChanges();

                _service = new SpecialQueryService(context, _mapper);
                Dictionary<ProjectDTO, int> dict = await _service.GetDictionaryOfUserProjects(1);
                Assert.Equal(1, dict.Values.First());
                Assert.Equal(0, dict.Values.Skip(1).First());
            }
        }
        [Fact]
        public async Task GetDictionary_IfNotExist_ReturnedEmpty()
        {
            using (var context = new LinqBinaryDbContext(ContextCleaner.CreateNewContextOptions()))
            {   //seed
                var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("Team.json"));
                var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("User.json"));
                var projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("Project.json"));
                var tasks = JsonConvert.DeserializeObject<List<ProjectTask>>(File.ReadAllText("ProjectTask.json"));

                context.Teams.AddRange(teams);
                context.Users.AddRange(users);
                context.Projects.AddRange(projects);
                context.ProjectTasks.AddRange(tasks);
                context.SaveChanges();

                _service = new SpecialQueryService(context, _mapper);
                Dictionary<ProjectDTO, int> dict = await _service.GetDictionaryOfUserProjects(50000);
                Assert.Empty(dict.Values);
            }
        }

        [Fact]
        public async Task GetUserTasks_ReturnedTasks()
        {
            using (var context = new LinqBinaryDbContext(ContextCleaner.CreateNewContextOptions()))
            {   //seed
                var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("Team.json"));
                var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("User.json"));
                var projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("Project.json"));
                var tasks = JsonConvert.DeserializeObject<List<ProjectTask>>(File.ReadAllText("ProjectTask.json"));

                context.Teams.AddRange(teams);
                context.Users.AddRange(users);
                context.Projects.AddRange(projects);
                context.ProjectTasks.AddRange(tasks);
                context.SaveChanges();

                _service = new SpecialQueryService(context, _mapper);
                var res = await _service.GetUsersTasks(1);
                Assert.Equal(2, res.Count());
                res = await _service.GetUsersTasks(2);
                Assert.Single(res);
                res = await _service.GetUsersTasks(3);
                Assert.Equal(2, res.Count());
            }
        }

        [Fact]
        public async Task GetUserFinishedTaskss_ReturnedTasks()
        {
            using (var context = new LinqBinaryDbContext(ContextCleaner.CreateNewContextOptions()))
            {   //seed
                var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("Team.json"));
                var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("User.json"));
                var projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("Project.json"));
                var tasks = JsonConvert.DeserializeObject<List<ProjectTask>>(File.ReadAllText("ProjectTask.json"));

                context.Teams.AddRange(teams);
                context.Users.AddRange(users);
                context.Projects.AddRange(projects);
                context.ProjectTasks.AddRange(tasks);
                context.SaveChanges();

                _service = new SpecialQueryService(context, _mapper);
                var res = await _service.GetUserFinishedTasks(1);
                Assert.Single(res);
                res = await _service.GetUserFinishedTasks(2);
                Assert.Single(res);
                res = await _service.GetUserFinishedTasks(3);
                Assert.Empty(res);
            }
        }

        [Fact]
        public async Task GetTeamsWithOldUsers()
        {
            using (var context = new LinqBinaryDbContext(ContextCleaner.CreateNewContextOptions()))
            {   //seed
                var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("Team.json"));
                var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("User.json"));
                var projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("Project.json"));
                var tasks = JsonConvert.DeserializeObject<List<ProjectTask>>(File.ReadAllText("ProjectTask.json"));

                context.Teams.AddRange(teams);
                context.Users.AddRange(users);
                context.Projects.AddRange(projects);
                context.ProjectTasks.AddRange(tasks);
                context.SaveChanges();

                _service = new SpecialQueryService(context, _mapper);
                var res = await _service.GetTeamsWithOldUsers();

                Assert.Equal("Retha", res.FirstOrDefault().Users.FirstOrDefault().FirstName);
                Assert.Equal("Hilton", res.FirstOrDefault().Users.Skip(1).FirstOrDefault().FirstName);
                Assert.Equal("Bud", res.FirstOrDefault().Users.Skip(2).FirstOrDefault().FirstName);

            }
        }

        [Fact]
        public async Task GetUsersWithTasks_CheckNameSorting()
        {
            using (var context = new LinqBinaryDbContext(ContextCleaner.CreateNewContextOptions()))
            {   //seed
                var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("Team.json"));
                var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("User.json"));
                var projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("Project.json"));
                var tasks = JsonConvert.DeserializeObject<List<ProjectTask>>(File.ReadAllText("ProjectTask.json"));

                context.Teams.AddRange(teams);
                context.Users.AddRange(users);
                context.Projects.AddRange(projects);
                context.ProjectTasks.AddRange(tasks);
                context.SaveChanges();

                _service = new SpecialQueryService(context, _mapper);
                var res = await _service.GetUsersWithTasks();
                //First user name starts on A
                Assert.Equal("A", res.First().User.FirstName.Substring(0, 1));
                //Last user name starts on Z
                Assert.Equal("Z", res.Last().User.FirstName.Substring(0, 1));
               
            }
        }
        [Fact]
        public async Task GetUsersWithTasks_CheckTaskSorting()
        {
            using (var context = new LinqBinaryDbContext(ContextCleaner.CreateNewContextOptions()))
            {   //seed
                var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("Team.json"));
                var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("User.json"));
                var projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("Project.json"));
                var tasks = JsonConvert.DeserializeObject<List<ProjectTask>>(File.ReadAllText("ProjectTask.json"));

                context.Teams.AddRange(teams);
                context.Users.AddRange(users);
                context.Projects.AddRange(projects);
                context.ProjectTasks.AddRange(tasks);
                context.SaveChanges();

                _service = new SpecialQueryService(context, _mapper);
                var res = await _service.GetUsersWithTasks();
                
                Assert.True(res.First().Tasks.First().Name.Length > res.First().Tasks.Last().Name.Length);
                Assert.True(res.Last().Tasks.First().Name.Length > res.Last().Tasks.Last().Name.Length);
                
            }
        }

        [Fact]
        public async Task GetUserInfoModel()
        {
            using (var context = new LinqBinaryDbContext(ContextCleaner.CreateNewContextOptions()))
            {   //seed
                var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("Team.json"));
                var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("User.json"));
                var projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("Project.json"));
                var tasks = JsonConvert.DeserializeObject<List<ProjectTask>>(File.ReadAllText("ProjectTask.json"));

                context.Teams.AddRange(teams);
                context.Users.AddRange(users);
                context.Projects.AddRange(projects);
                context.ProjectTasks.AddRange(tasks);
                context.SaveChanges();

                _service = new SpecialQueryService(context, _mapper);
                var res = await _service.GetUserInfoModel(1);

                Assert.Equal(1, res.ProjectTasks);
                Assert.Equal(2, res.CanceledOrUnFinishedTasks);
            }
        }

        [Fact]
        public async Task GetProjectsInfo()
        {
            using (var context = new LinqBinaryDbContext(ContextCleaner.CreateNewContextOptions()))
            {   //seed
                var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("Team.json"));
                var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("User.json"));
                var projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("Project.json"));
                var tasks = JsonConvert.DeserializeObject<List<ProjectTask>>(File.ReadAllText("ProjectTask.json"));

                context.Teams.AddRange(teams);
                context.Users.AddRange(users);
                context.Projects.AddRange(projects);
                context.ProjectTasks.AddRange(tasks);
                context.SaveChanges();

                _service = new SpecialQueryService(context, _mapper);

                var res = await _service.GetProjectsInfo();
                Assert.Equal(5, res.Last().UsersInTeam);
                Assert.True(res.Last().TaskWithLongestDescr.Description.Length > 35);
                Assert.True(res.Last().TaskWithShortestName.Name.Length < 35);
            }
        }
    }
}

