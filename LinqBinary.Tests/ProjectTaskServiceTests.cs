﻿using AutoMapper;
using LinqBinary.BLL.Services;
using LinqBinary.BLL.Services.SpecialQuery;
using LinqBinary.Common.DTO;
using LinqBinary.DAL.Context;
using LinqBinary.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using LinqBinary.Tests.Services;
using LinqBinary.Common.DTO.Enums;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LinqBinary.Tests
{
    public class ProjectTaskServiceTests
    {
        IMapper _mapper;
        ProjectTaskService _service;
        public ProjectTaskServiceTests()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ProjectTask, ProjectTaskDTO>().ForMember(m => m.State, src => src.MapFrom(dest => dest.State));
                cfg.CreateMap<ProjectTaskDTO, ProjectTask>().ForMember(m => m.State, src => src.MapFrom(dest => dest.State))
                    .ForMember(m => m.Performer, src => src.Ignore())
                    .ForMember(m => m.Project, src => src.Ignore());

            });
            _mapper = config.CreateMapper();

        }


        [Fact]
        public async void ChangeTaskStatus()
        {
            using (var context = new LinqBinaryDbContext(ContextCleaner.CreateNewContextOptions()))
            {
                var task = new ProjectTask()
                {
                    Id = 1,
                    Name = "Test",
                    Description = "TestDescr",
                    CreationDate = DateTime.Now,
                    PerformerId = null,
                    ProjectId = null,
                    State = DAL.Enums.TaskState.Created
                };
                context.ProjectTasks.Add(task);
                context.SaveChanges();
                _service = new ProjectTaskService(context, _mapper);

                var entityDto = await _service.Get(1);
                entityDto.State = TaskStateDTO.Finished;
                entityDto.FinishedAt = DateTime.Now;
                var res = await _service.Update(entityDto);


                Assert.Equal(entityDto.Id, res.Id);
                Assert.Equal(entityDto.Name, res.Name);
                Assert.Equal(entityDto.Description, res.Description);
                Assert.Equal(entityDto.State, res.State);
                Assert.Equal(entityDto.CreationDate, entityDto.CreationDate);
                Assert.Equal(entityDto.FinishedAt, entityDto.FinishedAt);

            }
        }
        [Fact]
        public async void ChangeTaskStatus_IfNotExist_ThenThrowNullReferenceException()
        {
            using (var context = new LinqBinaryDbContext(ContextCleaner.CreateNewContextOptions()))
            {
                _service = new ProjectTaskService(context, _mapper);

                await Assert.ThrowsAsync<NullReferenceException>(() =>  _service.Update(new ProjectTaskDTO()));
            }
        }
    }
}
