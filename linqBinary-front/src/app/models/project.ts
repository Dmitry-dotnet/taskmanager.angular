export interface Project {
    id:number,
    name:string,
    description: string,
    deadline: Date,
    teamId: number,
    authorId: number
}
