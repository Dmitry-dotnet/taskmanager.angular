export interface User {
    id : number, 
    firstName: string,
    lastName: string,
    email: string,
    birthDayDate : Date
    registrationDate: Date
    teamId?: number,
    
}
