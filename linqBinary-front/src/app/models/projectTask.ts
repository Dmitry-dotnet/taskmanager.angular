import { TaskState } from "./enums/taskState.enum";

export interface ProjectTask {
    id:number,
    name: string,
    description: string,
    finishedAt: Date,
    state: TaskState,
    projectId: number,
    performerId: number,
    creationDate: Date,
}
