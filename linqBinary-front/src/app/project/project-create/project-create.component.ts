import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Project } from 'src/app/models/project';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.css']
})
export class ProjectCreateComponent implements OnInit {

  
  @Input() project: Project = {} as Project;
  @Output() public creating: EventEmitter<Project> = new EventEmitter<Project>();
  @Output() public close: EventEmitter<Project> = new EventEmitter<Project>();
  @Output() public edit: EventEmitter<Project> = new EventEmitter<Project>();
  ProjectForm : FormGroup;
  private eventsSubscription: Subscription;
  @Input() events: Observable<void> = new Observable<void>();
  
  constructor(private dialog: DialogService) { }

  ngOnInit(): void {
    this.ProjectForm = new FormGroup({
      "name" : new FormControl(this.project.name, 
        [
          Validators.required,
        ]),
        "description": new FormControl(this.project.description),
        "authorId" : new FormControl(this.project.authorId, [
          Validators.required,
        ]),
        "teamId" : new FormControl(this.project.teamId,
          [
            Validators.required
          ]),
        "deadline" : new FormControl(this.project.deadline)
    });
    this.eventsSubscription = this.events.subscribe(() => this.canComDeactivate());
  }
      submit(){
        const newProject :Project = this.ProjectForm.value;
        this.creating.emit(newProject);
        this.ProjectForm.reset();
      }
      
      cancelation(){
        this.close.emit();
      }

      update(){
        const updatedProject : Project = this.ProjectForm.value;
        updatedProject.id = this.project.id;
        this.edit.emit(updatedProject);
        this.close.emit();
      }

      public canComDeactivate(): Observable<boolean> | boolean {
        if (this.ProjectForm.dirty) {
            return this.dialog.confirm('Cancel creating for Project?');
        }
        return true;
    }

}
