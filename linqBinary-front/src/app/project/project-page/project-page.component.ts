import { Component, OnInit, EventEmitter } from '@angular/core';
import { Project } from "../../models/project";
import { ProjectService } from "../../services/project.service";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.css']
})
export class ProjectPageComponent implements OnInit {
  events: EventEmitter<void> = new EventEmitter<void>();


  projects: Project[]=[];

  constructor(private projectService : ProjectService) { }

  ngOnInit(): void {
    this.loadProjects();
  }

  loadProjects(){
    this.projectService.getProjects()
    .subscribe(
      (resp) => {
        this.projects = resp.body;
        this.projects.sort((x,y) => y.id-x.id );
      },
    (error) => console.log(error)
    );
  }
  onProjectCreate(project : Project){
    this.projectService.createProject(project)
    .subscribe(
      ()=>{
        this.projects.push(project);
        this.loadProjects();
      },
      (error) => console.error(error)
    );
  }
  canComDeactivate(): Observable<boolean> | boolean {
    this.events.emit();
    return true;
}
}
