import { Component, OnInit, Input } from '@angular/core';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

  @Input('projects') projectList: Project[];
  
  constructor(private projectService: ProjectService) { }
  ngOnInit(): void {
  }

  onProjectDelete(project: Project){
    this.projectService.deleteProject(project.id)
    .subscribe(
      (success) => {
        const index = this.projectList.indexOf(project);
        if (index > -1){
          this.projectList.splice(index, 1);
          console.log("success");
        }
      },
      (error) => console.error(error)
      );
  }

  onTaskEdit(task: Project){
    this.projectService.updateProject(task)
    .subscribe(
      () => {
          const old = this.projectList.findIndex(x => x.id == task.id)
          // const index = this.projectList.indexOf(old);
          if(old > -1){
            this.projectList[old] = task;
            this.sortTeamsArray(this.projectList);
            console.log("success");
          }
        
      }
    )
  }
  
  private sortTeamsArray(array: Project[]): Project[] {
    return array.sort((a, b) => b.id - a.id);
  }
}
