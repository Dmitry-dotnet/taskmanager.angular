import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { Project } from "src/app/models/project";

@Component({
  selector: "app-project-details",
  templateUrl: "./project-details.component.html",
  styleUrls: ["./project-details.component.css"]
})
export class ProjectDetailsComponent implements OnInit {

  @Input("project") project : Project;
  @Output() public deleting: EventEmitter<Project> = new EventEmitter<Project>();
  @Output() public editing: EventEmitter<Project> = new EventEmitter<Project>();
  editingMode : boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  deleteProject(){
    this.deleting.emit(this.project);
  }


  editingSwitch(){
    if(this.editingMode==false){
      this.editingMode=true
      return;
    }
    this.editingMode=false
  }
  changeProject(updatedProject : Project){
    console.log(updatedProject);
    this.editing.emit(updatedProject);
  }

}
