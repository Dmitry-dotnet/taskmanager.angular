import { Component, OnInit, EventEmitter } from '@angular/core';
import {Team} from "../../models/team"
import { TeamService } from "../../services/team-service.service";
import { Observable } from 'rxjs';
@Component({
  selector: 'app-team-page',
  templateUrl: './team-page.component.html',
  styleUrls: ['./team-page.component.css']
})
export class TeamPageComponent implements OnInit {
  events: EventEmitter<void> = new EventEmitter<void>();

  teams: Team[]=[];

  constructor(private teamService : TeamService) { }

  ngOnInit(): void {
    this.loadTeams();
  }

  loadTeams(){
    this.teamService.getTeams()
    .subscribe(
      (resp) => {
        this.teams = resp.body;
        this.teams.sort((x,y) => y.id-x.id );
      },
    (error) => console.log(error)
    );
  }

  onTeamCreate(team : Team){
    this.teamService.createTeam(team)
    .subscribe(
      ()=>{
        this.teams.push(team);
        this.loadTeams();
      },
      (error) => console.error(error)
    );
  }
  
  canComDeactivate(): Observable<boolean> | boolean {
    this.events.emit();
    return true;
}

}
