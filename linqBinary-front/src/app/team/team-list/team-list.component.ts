import { Component, OnInit, Input } from '@angular/core';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team-service.service';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {
  
  @Input('teams') teamsList: Team[];
  
  constructor(private teamService: TeamService) { }
  ngOnInit(): void {
  }

  onTeamDelete(team: Team){
    this.teamService.deleteTeam(team.id)
    .subscribe(
      (success) => {
        const index = this.teamsList.indexOf(team);
        if (index > -1){
          this.teamsList.splice(index, 1);
          console.log("success");
        }
      },
      (error) => console.error(error)
      );
  }
  
  onTeamEdit(team: Team){
    this.teamService.updateTeam(team)
    .subscribe(
      () => {
          const old = this.teamsList.findIndex(x => x.id == team.id)
          // const index = this.TeamsList.indexOf(old);
          if(old > -1){
            this.teamsList[old] = team;
            this.sortTeamsArray(this.teamsList);
            console.log("success");
          }
        
      }
    )
  }
  
  private sortTeamsArray(array: Team[]): Team[] {
    return array.sort((a, b) => b.id - a.id);
  }
}
