import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Team } from 'src/app/models/team';

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.css']
})
export class TeamDetailsComponent implements OnInit {
  @Input() team : Team;
  @Output() public deleting: EventEmitter<Team> = new EventEmitter<Team>();
  @Output() public editing: EventEmitter<Team> = new EventEmitter<Team>();
  editingMode : boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  deleteTeam(){
    this.deleting.emit(this.team);
  }

  editingSwitch(){
    if(this.editingMode==false){
      this.editingMode=true
      return;
    }
    this.editingMode=false
  }
  changeTeam(updatedTeam : Team){
    console.log(updatedTeam);
    this.editing.emit(updatedTeam);
  }
}
