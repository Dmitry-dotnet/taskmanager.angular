import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Team } from 'src/app/models/team';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-team-create',
  templateUrl: './team-create.component.html',
  styleUrls: ['./team-create.component.css']
})
export class TeamCreateComponent implements OnInit {

  @Input() team: Team = {} as Team;
  @Output() public creating: EventEmitter<Team> = new EventEmitter<Team>();
  @Output() public close: EventEmitter<Team> = new EventEmitter<Team>();
  @Output() public edit: EventEmitter<Team> = new EventEmitter<Team>();
  teamForm : FormGroup;
  private eventsSubscription: Subscription;
  @Input() events: Observable<void> = new Observable<void>();
  
  constructor(private dialog: DialogService) { }

  ngOnInit(): void {
    this.teamForm = new FormGroup({
      "name" : new FormControl(this.team.name, 
        [
          Validators.required,
        ])
    });
    this.eventsSubscription = this.events.subscribe(() => this.canComDeactivate());

  }
      submit(){
        const newTeam :Team = this.teamForm.value;
        this.creating.emit(newTeam);
        this.teamForm.reset();
      }
      
      cancelation(){
        this.close.emit();
      }

      update(){
        const updatedTeam : Team = this.teamForm.value;
        updatedTeam.id = this.team.id;
        this.edit.emit(updatedTeam);
        this.close.emit();
      }

      public canComDeactivate(): Observable<boolean> | boolean {
        if (this.teamForm.dirty) {
            return this.dialog.confirm('Cancel creating for Team?');
        }
        return true;
    }

}
