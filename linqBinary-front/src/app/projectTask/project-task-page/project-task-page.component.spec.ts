import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectTaskPageComponent } from './project-task-page.component';

describe('ProjectTaskPageComponent', () => {
  let component: ProjectTaskPageComponent;
  let fixture: ComponentFixture<ProjectTaskPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectTaskPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectTaskPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
