import { Component, OnInit, EventEmitter } from '@angular/core';
import { ProjectTask } from 'src/app/models/projectTask';
import { ProjectTaskService } from 'src/app/services/project-task.service';
import { TaskState } from 'src/app/models/enums/taskState.enum';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-project-task-page',
  templateUrl: './project-task-page.component.html',
  styleUrls: ['./project-task-page.component.css']
})
export class ProjectTaskPageComponent implements OnInit {
  events: EventEmitter<void> = new EventEmitter<void>();

  projectTasks: ProjectTask[]=[];

  constructor(private projectTaskService : ProjectTaskService) { }

  ngOnInit(): void {
    this.loadProjectTasks();
  }

  loadProjectTasks(){
    this.projectTaskService.getProjectTasks()
    .subscribe(
      (resp) => {
        this.projectTasks = resp.body;
        this.projectTasks.sort((x,y) => y.id-x.id );
      },
    (error) => console.log(error)
    );
  }

  onProjectTaskCreate(projectTask : ProjectTask){
    console.log(projectTask.name + " received");
    this.projectTaskService.createProjectTask(projectTask)
    .subscribe(
      ()=>{
        this.projectTasks.push(projectTask);
        this.loadProjectTasks();
      },
      (error) => console.error(error)
    );
  }

  canComDeactivate(): Observable<boolean> | boolean {
    this.events.emit();
    return true;
}


}
