import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProjectTask } from 'src/app/models/projectTask';
import { TaskState } from 'src/app/models/enums/taskState.enum';

@Component({
  selector: 'app-project-task-details',
  templateUrl: './project-task-details.component.html',
  styleUrls: ['./project-task-details.component.css']
})
export class ProjectTaskDetailsComponent implements OnInit {
  @Input('projectTask') projectTask : ProjectTask;
  @Output() public deleting: EventEmitter<ProjectTask> = new EventEmitter<ProjectTask>();
  @Output() public editing: EventEmitter<ProjectTask> = new EventEmitter<ProjectTask>();
  editingMode : boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  deleteProjectTask(){
    this.deleting.emit(this.projectTask);
  }


  displayState() : string{
    const state = this.projectTask.state;
    return TaskState[state];
  }

  editingSwitch(){
    if(this.editingMode==false){
      this.editingMode=true
      return;
    }
    this.editingMode=false
  }
  changeTask(updatedTask : ProjectTask){
    console.log(updatedTask);
    this.editing.emit(updatedTask);
  }
}
