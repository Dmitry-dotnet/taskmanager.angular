import { Component, OnInit, Input } from '@angular/core';
import { ProjectTask } from "../../models/projectTask";
import { ProjectTaskService } from "../../services/project-task.service";

@Component({
  selector: 'app-project-task-list',
  templateUrl: './project-task-list.component.html',
  styleUrls: ['./project-task-list.component.css']
})
export class ProjectTaskListComponent implements OnInit {

  
  @Input('projectTasks') taskList: ProjectTask[];
  
  constructor(private projectTaskService: ProjectTaskService) { }
  ngOnInit(): void {
  }

  onTaskDelete(task: ProjectTask){
    this.projectTaskService.deleteProjectTask(task.id)
    .subscribe(
      (success) => {
        const index = this.taskList.indexOf(task);
        if (index > -1){
          this.taskList.splice(index, 1);
          console.log("success");
        }
      },
      (error) => console.error(error)
      );
  }

  onTaskEdit(task: ProjectTask){
    this.projectTaskService.updateProjectTask(task)
    .subscribe(
      () => {
          const old = this.taskList.findIndex(x => x.id == task.id)
          // const index = this.taskList.indexOf(old);
          if(old > -1){
            this.taskList[old] = task;
            this.sortTeamsArray(this.taskList);
            console.log("success");
          }
        
      }
    )
  }
  
  private sortTeamsArray(array: ProjectTask[]): ProjectTask[] {
    return array.sort((a, b) => b.id - a.id);
  }

}
