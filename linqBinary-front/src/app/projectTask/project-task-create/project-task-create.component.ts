import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ProjectTask } from 'src/app/models/projectTask';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TaskState } from 'src/app/models/enums/taskState.enum';
import { Subscription, Observable } from 'rxjs';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-project-task-create',
  templateUrl: './project-task-create.component.html',
  styleUrls: ['./project-task-create.component.css']
})
export class ProjectTaskCreateComponent implements OnInit {

  @Input() projectTask: ProjectTask = {} as ProjectTask;
  @Output() public creating: EventEmitter<ProjectTask> = new EventEmitter<ProjectTask>();
  @Output() public close: EventEmitter<ProjectTask> = new EventEmitter<ProjectTask>();
  @Output() public edit: EventEmitter<ProjectTask> = new EventEmitter<ProjectTask>();
  ProjectTaskForm : FormGroup;
  state = TaskState; 
  stateValues: Array<string> = Object.keys(this.state).filter(key => isNaN(+key));
  private eventsSubscription: Subscription;
  @Input() events: Observable<void> = new Observable<void>();
  
  constructor(private dialog: DialogService) { }



  ngOnInit(): void {
    this.ProjectTaskForm = new FormGroup({
      "name" : new FormControl(this.projectTask.name, 
        [
          Validators.required,
        ]),
        "description": new FormControl(this.projectTask.description),
        "performerId" : new FormControl(this.projectTask.performerId, [
          Validators.required,
        ]),
        "projectId" : new FormControl(this.projectTask.projectId,
          [
            Validators.required
          ]),
          "state" : new FormControl(this.projectTask.state)
    });
    this.eventsSubscription = this.events.subscribe(() => this.canComDeactivate());


  }
      submit(){
        const newProjectTask :ProjectTask = this.ProjectTaskForm.value;
        this.creating.emit(newProjectTask);
        this.ProjectTaskForm.reset();
      }
      
      cancelation(){
        this.close.emit();
      }

      update(){
        const updatedProjectTask : ProjectTask = this.ProjectTaskForm.value;
        updatedProjectTask.id = this.projectTask.id;
        this.edit.emit(updatedProjectTask);
        this.close.emit();
      }

      public canComDeactivate(): Observable<boolean> | boolean {
        if (this.ProjectTaskForm.dirty) {
            return this.dialog.confirm('Cancel creating for Task?');
        }
        return true;
    }
}
