import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ukrDate'
})
export class UkrDatePipe implements PipeTransform {

  transform(value: Date, ...args: unknown[]) {
    if (value){
      const date = new Date(value);
      const options = { year: 'numeric', month: 'long', day: 'numeric' };
      return `${date.toLocaleDateString("uk-UA", options)}`;
    }
    return "No data";
  } 

}
