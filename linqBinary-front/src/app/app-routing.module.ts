import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserPageComponent } from './user/user-page/user-page.component';
import { TeamPageComponent } from './team/team-page/team-page.component';
import { ProjectTaskPageComponent } from "./projectTask/project-task-page/project-task-page.component";
import { ProjectPageComponent } from "./project/project-page/project-page.component";
import { UserCreateComponent } from './user/user-create/user-create.component';
import { CanDeactivateGuard } from './guards/can-deactivate.guard';
import { UserDetailsComponent } from './user/user-details/user-details.component';
import { UserListComponent } from './user/user-list/user-list.component';

const routes : Routes = [
  { path: "users", component: UserPageComponent, canDeactivate:[CanDeactivateGuard]},
  { path: "teams", component: TeamPageComponent, canDeactivate:[CanDeactivateGuard]},
  { path: "tasks", component: ProjectTaskPageComponent, canDeactivate:[CanDeactivateGuard]},
  { path: "projects", component: ProjectPageComponent, canDeactivate:[CanDeactivateGuard]},
  { path: "**", redirectTo: "users"},
] 

@NgModule({
  imports: [RouterModule.forRoot(routes),
    CommonModule
  ],
  exports:[RouterModule],
  providers:[CanDeactivateGuard]
})
export class AppRoutingModule { }
