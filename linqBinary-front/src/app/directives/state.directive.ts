import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appState]'
})
export class StateDirective  {

  constructor(private element : ElementRef) {
    
   }

   ngAfterViewInit() {
     if(this.element.nativeElement.innerText.includes("Created")){
      this.element.nativeElement.style.backgroundColor = "#33FFBD";
    }
    if(this.element.nativeElement.innerText.includes("Started")){
      this.element.nativeElement.style.backgroundColor = "#DBFF33";
    }
    if(this.element.nativeElement.innerText.includes("Finished")){
      this.element.nativeElement.style.backgroundColor = "#75FF33";
    }
    if(this.element.nativeElement.innerText.includes("Canceled")){
      this.element.nativeElement.style.backgroundColor = "#FFBD33";
    }
  }

}
