import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user-service.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  @Input('users') usersList: User[];
  constructor(private userService: UserService) { }
  ngOnInit(): void {
  }

onUserDelete(user : User){
  this.userService.deleteUser(user.id)
  .subscribe(
    (success) => {
      const index = this.usersList.indexOf(user);
      if (index > -1){
        this.usersList.splice(index, 1);
        console.log("success");
      }
    },
    (error) => console.error(error)
    );

}


onUserEdit(user: User){
  this.userService.updateUser(user)
  .subscribe(
    () => {
        const old = this.usersList.findIndex(x => x.id == user.id)
        if(old > -1){
          this.usersList[old] = user;
          this.sortUsersArray(this.usersList);
        }
      
    }
  )
}

private sortUsersArray(array: User[]): User[] {
  return array.sort((a, b) => b.id - a.id);
}
}
