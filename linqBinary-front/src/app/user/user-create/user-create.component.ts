import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { DialogService } from "../../services/dialog.service";
import { Observable, Subscription } from 'rxjs';
import { CanComponentDeactivate } from 'src/app/guards/can-deactivate.guard';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit, OnDestroy, CanComponentDeactivate {

  @Input() user: User = {} as User;
  @Output() public creating: EventEmitter<User> = new EventEmitter<User>();
  @Output() public close: EventEmitter<User> = new EventEmitter<User>();
  @Output() public edit: EventEmitter<User> = new EventEmitter<User>();
  public userForm : FormGroup;
  private eventsSubscription: Subscription;
  @Input() events: Observable<void> = new Observable<void>();

  constructor(private dialog : DialogService) { }

  ngOnInit(): void {
    this.userForm = new FormGroup({
      "firstName" : new FormControl(this.user.firstName, 
        [
          Validators.required,
        ]),
        "lastName" : new FormControl(this.user.lastName, [
          Validators.required,
        ]),
        "email" : new FormControl(this.user.email,
          [
            Validators.email
          ]),
          "birthdayDate" : new FormControl(this.user.birthDayDate),
          "teamId" : new FormControl(this.user.teamId, [
            Validators.required
          ])
        });

        this.eventsSubscription = this.events.subscribe(() => this.canComDeactivate());
  }

  ngOnDestroy() {
    this.eventsSubscription.unsubscribe();
  }
      submit(){
        const newUser :User = this.userForm.value;
        this.creating.emit(newUser);
        this.userForm.reset();
      }
      
      cancelation(){
        this.close.emit();
      }

      update(){
        const updatedUser : User = this.userForm.value;
        updatedUser.id = this.user.id;
        this.edit.emit(updatedUser);
        this.close.emit();
      }
      
      public canComDeactivate(): Observable<boolean> | boolean {
        if (this.userForm.dirty) {
            return this.dialog.confirm('Cancel creating for team?');
        }
        return true;
    }

}
