import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from "../../models/user";
import { DialogService } from 'src/app/services/dialog.service';
import { Observable } from 'rxjs';
import { CanComponentDeactivate } from 'src/app/guards/can-deactivate.guard';
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  @Input() user : User;
  @Output() public deleting: EventEmitter<User> = new EventEmitter<User>();
  @Output() public editing: EventEmitter<User> = new EventEmitter<User>();
  editingMode : boolean = false;

  constructor(private dialog : DialogService) { 
    
  }

  ngOnInit(): void {
  }

  deleteUser(){
    this.deleting.emit(this.user);
  }

  editingSwitch(){
    if(this.editingMode==false){
      this.editingMode=true
      return;
    }
    this.editingMode=false
  }
  changeUser(updatedUser : User){
    console.log(updatedUser);
    this.editing.emit(updatedUser);
  }

 
}
