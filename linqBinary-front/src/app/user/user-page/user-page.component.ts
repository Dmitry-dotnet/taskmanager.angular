import { Component, OnInit, EventEmitter } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from "../../services/user-service.service";
import { CanComponentDeactivate } from 'src/app/guards/can-deactivate.guard';
import { UserCreateComponent } from '../user-create/user-create.component';
import { Observable, Subject } from 'rxjs';
@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit, CanComponentDeactivate {
  events: EventEmitter<void> = new EventEmitter<void>();

  users: User[]=[];

  constructor(private userService : UserService) { }

  ngOnInit(): void {
    this.loadUsers();
  }


  loadUsers(){
    this.userService.getUsers()
    .subscribe(
      (resp) => {
        this.users = resp.body;
        this.users.sort((x,y) => y.id-x.id );
      },
    (error) => console.log(error)
    );
  }

  onUserCreate(user : User){
    this.userService.createUser(user)
    .subscribe(
      ()=>{
        this.users.push(user);
        this.loadUsers();
      },
      (error) => console.error(error)
    );
  }

  canComDeactivate(): Observable<boolean> | boolean {
    this.events.emit();
    return true;
}
}
