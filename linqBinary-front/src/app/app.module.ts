import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {MatCardModule} from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserDetailsComponent } from './user/user-details/user-details.component';
import { HttpInternalService } from './services/http-internal.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserCreateComponent } from './user/user-create/user-create.component';
import { AppRootComponent } from './app-root/app-root.component';
import { AppRoutingModule } from './app-routing.module';
import { TeamPageComponent } from './team/team-page/team-page.component';
import { TeamListComponent } from './team/team-list/team-list.component';
import { TeamDetailsComponent } from './team/team-details/team-details.component';
import { TeamCreateComponent } from './team/team-create/team-create.component';
import { ProjectTaskPageComponent } from './projectTask/project-task-page/project-task-page.component';
import { ProjectTaskListComponent } from './projectTask/project-task-list/project-task-list.component';
import { ProjectTaskDetailsComponent } from './projectTask/project-task-details/project-task-details.component';
import { UserPageComponent } from './user/user-page/user-page.component';
import { ProjectTaskCreateComponent } from './projectTask/project-task-create/project-task-create.component';
import { ProjectPageComponent } from './project/project-page/project-page.component';
import { ProjectListComponent } from './project/project-list/project-list.component';
import { ProjectDetailsComponent } from './project/project-details/project-details.component';
import { ProjectCreateComponent } from './project/project-create/project-create.component';
import { StateDirective } from './directives/state.directive';
import { UkrDatePipe } from './pipes/ukr-date.pipe';
import { CanDeactivateGuard } from "../app/guards/can-deactivate.guard";

@NgModule({
  declarations: [
    AppComponent, 
    UserListComponent, 
    UserDetailsComponent, 
    UserPageComponent, 
    UserCreateComponent, 
    AppRootComponent, 
    TeamPageComponent, 
    TeamListComponent, 
    TeamDetailsComponent, 
    TeamCreateComponent, 
    ProjectTaskPageComponent, 
    ProjectTaskListComponent, 
    ProjectTaskDetailsComponent, 
    ProjectTaskCreateComponent, 
    ProjectPageComponent, 
    ProjectListComponent, 
    ProjectDetailsComponent, 
    ProjectCreateComponent, 
    StateDirective, UkrDatePipe,  

  ],
  imports: [
    BrowserModule, HttpClientModule, BrowserAnimationsModule, 
    MatCardModule, FormsModule, ReactiveFormsModule, AppRoutingModule
  ],
  providers: [HttpClientModule, HttpInternalService, CanDeactivateGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
