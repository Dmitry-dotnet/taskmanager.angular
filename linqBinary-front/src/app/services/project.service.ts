import { Injectable } from '@angular/core';
import { Project } from "../models/project";
import { HttpInternalService } from './http-internal.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  urlPrefix : string = "api/projects"

  constructor(private httpService : HttpInternalService) { }
  
  users : Project[] = [];
  
  public getProjects(){
    return this.httpService.getFullRequest<Project[]>(`${this.urlPrefix}`);
  }
  
  public deleteProject(id : number){
    return this.httpService.deleteFullRequest(`${this.urlPrefix}/${id}`);
  }
  
  public createProject(user: Project){
    return this.httpService.postFullRequest(`${this.urlPrefix}`, user);
  }
  
  public updateProject(user: Project){
    return this.httpService.putFullRequest(`${this.urlPrefix}`, user);
  }
}
