import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Team } from '../models/team';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  urlPrefix : string = "api/teams"

constructor(private httpService : HttpInternalService) { }

users : Team[] = [];

public getTeams(){
  return this.httpService.getFullRequest<Team[]>(`${this.urlPrefix}`);
}

public deleteTeam(id : number){
  return this.httpService.deleteFullRequest(`${this.urlPrefix}/${id}`);
}

public createTeam(user: Team){
  return this.httpService.postFullRequest(`${this.urlPrefix}`, user);
}

public updateTeam(user: Team){
  return this.httpService.putFullRequest(`${this.urlPrefix}`, user);
}
}
