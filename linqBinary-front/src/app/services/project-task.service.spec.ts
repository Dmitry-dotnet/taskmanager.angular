/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProjectTaskService } from './project-task.service';

describe('Service: ProjectTask', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProjectTaskService]
    });
  });

  it('should ...', inject([ProjectTaskService], (service: ProjectTaskService) => {
    expect(service).toBeTruthy();
  }));
});
