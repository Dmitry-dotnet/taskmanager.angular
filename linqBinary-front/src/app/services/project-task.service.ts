import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { ProjectTask } from "../models/projectTask";
@Injectable({
  providedIn: 'root'
})
export class ProjectTaskService {
  urlPrefix : string = "api/tasks"

  constructor(private httpService : HttpInternalService) { }
  
  users : ProjectTask[] = [];
  
  public getProjectTasks(){
    return this.httpService.getFullRequest<ProjectTask[]>(`${this.urlPrefix}`);
  }
  
  public deleteProjectTask(id : number){
    return this.httpService.deleteFullRequest(`${this.urlPrefix}/${id}`);
  }
  
  public createProjectTask(user: ProjectTask){
    return this.httpService.postFullRequest(`${this.urlPrefix}`, user);
  }
  
  public updateProjectTask(user: ProjectTask){
    return this.httpService.putFullRequest(`${this.urlPrefix}`, user);
  }
}
