import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { User } from "../models/user";
@Injectable({
  providedIn: 'root'
})
export class UserService {
  urlPrefix : string = "api/users"

constructor(private httpService : HttpInternalService) { }

users : User[] = [];

public getUsers(){
  return this.httpService.getFullRequest<User[]>(`${this.urlPrefix}`);
}

public deleteUser(id : number){
  return this.httpService.deleteFullRequest(`${this.urlPrefix}/${id}`);
}

public createUser(user: User){
  return this.httpService.postFullRequest(`${this.urlPrefix}`, user);
}

public updateUser(user: User){
  return this.httpService.putFullRequest(`${this.urlPrefix}`, user);
}

}
